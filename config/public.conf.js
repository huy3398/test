module.exports = {
  url: {
    api: 'http://localhost:3000/api',
    base: 'http://localhost:3000',
    apiV2: 'http://api.watacorp.vn:5757/api',
    baseV2: 'http://api.watacorp.vn:5757',
  },
  company: {
    name: 'Wata Solutions',
    description: '',
    logo: '/img/Logo.svg',
    geo: {
      latitude: '10.8094287232523',
      longtitude: '106.66417256830795',
    },
    address: {
      streetAddress: '39B Trường Sơn, Phường 2, Tân Bình',
      region: 'Hồ Chí Minh City',
      locality: 'Hồ Chí Minh City',
      postalCode: '70000',
      embedUrl:
        'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.0272010381846!2d106.66199461462283!3d10.80922849229927!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529b65c14c9c7%3A0xfcd882bf03b4c82d!2sWATA%20SOLUTIONS!5e0!3m2!1sen!2s!4v1619944711709!5m2!1sen!2s',
    },
  },
  contact: {
    email: 'info@watacorp.com',
    phone: '028 3636 9248',
    chatUs: 'Chat with us',
    skype: 'https://join.skype.com/invite/pJKujeOekE1u',
    linkedin: 'https://vn.linkedin.com/company/watacorp',
    facebook: 'https://www.facebook.com/watasolutions',
    whatsapp: '+84 028 3636 9248',
  },
  media: {
    about: 'https://www.youtube.com/embed/uDdDO83qlZs',
  },
  integration: {
    gtm: { containerId: 'GTM-5X6V5C3' },
  },
}
