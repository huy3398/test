module.exports = {
  redirects: {
    '/about-us': '/about',
    '/career': '/careers',
    '/home': '/',
    '/home-2': '/',
    '/our-work': '/works',
    '/partners': '/about',
    '/mobile-applictaion': '/services',
    '/software-testing': '/services',
    '/ui-ux-design': '/services',
    '/websites': '/services',
  },
}
