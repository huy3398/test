module.exports = [
  {
    title:
      'Downgraded Hue temple recalls era of Nguyen Dynasty ancestor worship',
    slug: 'technical-article-1',
    image: '/img/articles/Techblog1.jpg',
    thumbnail: '/articles/Techblog1',
    author: { id: 1, name: 'Mark' },
    excerpt:
      "An excerpt from her new thriller will appear in this weekend's magazine.",
    content:
      "Born in Albuquerque and raised in Houston and later Miami, Bezos graduated from Princeton University in 1986. He holds a degree in electrical engineering and computer science. He worked on Wall Street in a variety of related fields from 1986 to early 1994. Bezos founded Amazon in late 1994, on a cross-country road trip from New York City to Seattle. The company began as an online bookstore and has since expanded to a wide variety of other e-commerce products and services, including video and audio streaming, cloud computing, and artificial intelligence. It is currently the world's largest online sales company, the largest Internet company by revenue, and the world's largest provider of virtual assistants[6] and cloud infrastructure services through its Amazon Web Services branch.",
    tag: [
      { id: 1, slug: 'angular', label: 'technical' },
      { id: 2, slug: 'responsive', label: 'frontend' },
    ],
    category: 1,
    status: 'Published',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '2', name: 'Hoang' },
    publishedDate: '2011-10-07T14:48:00.000Z',
    numberOfView: 3,
  },
  {
    title:
      'Singapore extends stay-home period for travelers from Vietnam to 21 days',
    slug: 'technical-article-2',
    image: '/img/articles/Techblog2.jpg',
    thumbnail: '/articles/Techblog2',
    author: { id: 1, name: 'Mark' },
    excerpt:
      'inVerita and its mobile development team continuously dig into the performance of cross-platform mobile solutions available on the market to answer the question which technology is best Flutter or React Native(or Native) for your product, maybe even career, that’s how Flutter vs React Native vs Native Part I emerged. Yes, it was quite controversial as one can state we weren’t using React Native to perform multiple calculations daily — that might be the case — but in this case, CPU heavy tasks are better performed by Flutter or Native apps...',
    content:
      "Born in Albuquerque and raised in Houston and later Miami, Bezos graduated from Princeton University in 1986. He holds a degree in electrical engineering and computer science. He worked on Wall Street in a variety of related fields from 1986 to early 1994. Bezos founded Amazon in late 1994, on a cross-country road trip from New York City to Seattle. The company began as an online bookstore and has since expanded to a wide variety of other e-commerce products and services, including video and audio streaming, cloud computing, and artificial intelligence. It is currently the world's largest online sales company, the largest Internet company by revenue, and the world's largest provider of virtual assistants[6] and cloud infrastructure services through its Amazon Web Services branch.",
    tag: [
      { id: 1, slug: 'angular', label: 'technical' },
      { id: 2, slug: 'responsive', label: 'frontend' },
    ],
    category: 2,
    status: 'Published',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '3', name: 'Quang' },
    publishedDate: '2011-10-06T13:48:00.000Z',
    numberOfView: 3,
  },
  {
    title:
      'Giving more tools to software engineers: the reorganization of the factory',
    slug: 'technical-article-3',
    image: '/img/articles/Techblog3.jpg',
    thumbnail: '/articles/Techblog3',
    author: { id: 1, name: 'Mark' },
    excerpt:
      'inVerita and its mobile development team continuously dig into the performance of cross-platform mobile solutions available on the market to answer the question which technology is best Flutter or React Native(or Native) for your product, maybe even career, that’s how Flutter vs React Native vs Native Part I emerged. Yes, it was quite controversial as one can state we weren’t using React Native to perform multiple calculations daily — that might be the case — but in this case, CPU heavy tasks are better performed by Flutter or Native apps.',
    content:
      "It's a popular attitude among developers to rant about our tools and how broken things are. Maybe I'm an optimistic person, because my viewpoint is the complete opposite! I had my first job as a software engineer in 1999, and in the last two decades I've seen software engineering changing in ways that have made us orders of magnitude more productive. Just some examples from things I've worked on or close to.",
    tag: [
      { id: 1, slug: 'angular', label: 'technical' },
      { id: 2, slug: 'responsive', label: 'frontend' },
    ],
    category: 1,
    status: 'Published',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '2', name: 'Hoang' },
    publishedDate: '2011-10-05T12:48:00.000Z',
    numberOfView: 3,
  },
  {
    title:
      'Giving more tools to software engineers: the reorganization of the factory',
    slug: 'technical-article-4',
    image: '/img/articles/Techblog1.jpg',
    thumbnail: '/articles/Techblog1',
    author: { id: 1, name: 'Mark' },
    excerpt:
      'inVerita and its mobile development team continuously dig into the performance of cross-platform mobile solutions available on the market to answer the question which technology is best Flutter or React Native(or Native) for your product, maybe even career, that’s how Flutter vs React Native vs Native Part I emerged. Yes, it was quite controversial as one can state we weren’t using React Native to perform multiple calculations daily — that might be the case — but in this case, CPU heavy tasks are better performed by Flutter or Native apps...',
    content:
      "It's a popular attitude among developers to rant about our tools and how broken things are. Maybe I'm an optimistic person, because my viewpoint is the complete opposite! I had my first job as a software engineer in 1999, and in the last two decades I've seen software engineering changing in ways that have made us orders of magnitude more productive. Just some examples from things I've worked on or close to.",
    tag: [
      { id: 1, slug: 'angular', label: 'technical' },
      { id: 2, slug: 'responsive', label: 'frontend' },
    ],
    category: 1,
    status: 'Published',
    creator: '2',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '2', name: 'Hoang' },
    publishedDate: '2011-10-04T11:48:00.000Z',
    numberOfView: 3,
  },
  {
    title:
      'Giving more tools to software engineers: the reorganization of the factory',
    slug: 'technical-article-5',
    image: '/img/articles/Techblog1.jpg',
    thumbnail: '/articles/Techblog1',
    author: { id: 1, name: 'Mark' },
    excerpt:
      'inVerita and its mobile development team continuously dig into the performance of cross-platform mobile solutions available on the market to answer the question which technology is best Flutter or React Native(or Native) for your product, maybe even career, that’s how Flutter vs React Native vs Native Part I emerged. Yes, it was quite controversial as one can state we weren’t using React Native to perform multiple calculations daily — that might be the case — but in this case, CPU heavy tasks are better performed by Flutter or Native apps...',
    content:
      "It's a popular attitude among developers to rant about our tools and how broken things are. Maybe I'm an optimistic person, because my viewpoint is the complete opposite! I had my first job as a software engineer in 1999, and in the last two decades I've seen software engineering changing in ways that have made us orders of magnitude more productive. Just some examples from things I've worked on or close to.",
    tag: [
      { id: 1, slug: 'angular', label: 'technical' },
      { id: 2, slug: 'responsive', label: 'frontend' },
    ],
    category: 2,
    status: 'Published',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '2', name: 'Hoang' },
    publishedDate: '2011-10-03T10:48:00.000Z',
    numberOfView: 3,
  },
  {
    title: 'Điện thoại Samsung Galaxy Note 20',
    slug: 'samsung-galaxy-note-20',
    image: '/img/articles/samsung-galaxy-note-20.jpg',
    thumbnail: '/articles/samsung-galaxy-note-20',
    author: { id: 1, name: 'Broze' },
    excerpt:
      'Tháng 8/2020, Samsung Galaxy Note 20 chính thức được lên kệ, với thiết kế camera trước nốt ruồi quen thuộc, cụm camera hình chữ nhật mới lạ cùng với việc sử dụng vi xử lý Exynos 990 cao cấp của chính Samsung chắc hẳn sẽ mang lại một trải nghiệm thú vị cùng hiệu năng mạnh mẽ.',
    content:
      'Điện thoại sở hữu thiết kế khung kim loại chắc chắn, mặt lưng nhựa bóng bẩy, kiểu dáng mạnh mẽ với những góc cạnh vuông vức nhưng vẫn mang lại cảm giác cầm nắm thoải mái..',
    tag: [
      { id: 1, slug: 'cong-nghe', label: 'Công nghệ' },
      { id: 2, slug: 'giao-duc', label: 'giáo dục' },
    ],
    category: 1,
    status: 'To be approved',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '1', name: 'Ngan' },
    publishedDate: '2011-10-02T14:48:00.000Z',
    numberOfView: 3,
  },
  {
    title: 'Điện thoại iPhone 12 Pro Max 128GB',
    slug: 'iphone-12-pro-max',
    image: '/img/articles/iphone-12-pro-max.jpg',
    thumbnail: '/articles/iphone-12-pro-max',
    author: { id: 1, name: 'Broze' },
    excerpt:
      'Nếu không có gì thay đổi, iPhone 12 Pro Max 128 GB siêu phẩm smartphone đang được mong chờ nhất của Apple sẽ được ra mắt và khoảng nửa cuối năm nay (2020). Hứa hẹn đây sẽ là một sự thay đổi ngoạn mục của iPhone cả về thiết kế lẫn hiệu năng.',
    content:
      'Theo chu kỳ cứ mỗi 3 năm thì iPhone lại thay đổi thiết kế và 2020 là năm đánh dấu cột mốc quan trong này, vì thế rất có thể đây là thời điểm các mẫu iPhone 12 sẽ có một sự thay đổi mạnh mẽ về thiết kế. iPhone 12 Pro Max sở hữu diện mạo mới mới với khung viền được vát thẳng và mạnh mẽ như trên iPad Pro 2020, chấm dứt hơn 6 năm với kiểu thiết kế bo cong trên iPhone 6 được ra mắt lần đầu tiên vào năm 2014.',
    tag: [
      { id: 1, slug: 'cong-nghe', label: 'Công nghệ' },
      { id: 2, slug: 'giao-duc', label: 'giáo dục' },
    ],
    category: 2,
    status: 'Publish',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '1', name: 'Ngan' },
    publishedDate: '2011-10-01T14:48:00.000Z',
    numberOfView: 3,
  },
  {
    title: 'Điện thoại Vivo Y12s',
    slug: 'vivo-y-12s',
    image: '/img/articles/vivo-y12s-xanh.jpg',
    thumbnail: '/articles/vivo-y12s-xanh',
    author: { id: 1, name: 'Broze' },
    excerpt:
      'Vivo Y12s, chiếc điện thoại đến từ nhà sản xuất Vivo, sở hữu một mức giá tầm trung, hiệu năng mạnh mẽ cùng viên pin dung lượng khủng giúp bạn tha hồ trải nghiệm giải trí, chơi game suốt ngày dài.',
    content:
      'Vivo Y12s được thiết kế theo phong cách hiện đại, nguyên khối, mặt lưng của máy được bo cong 2.5D, sắc màu gradient thời thượng, kết hợp với thân máy mỏng nhẹ tạo cảm giác thoải mái và cao cấp khi cầm trên tay. Trọng lượng máy nhẹ (chỉ khoảng 191 g) cùng với đó là khả năng chống bám mồ hôi và dấu vân tay tốt.',
    tag: [
      { id: 1, slug: 'cong-nghe', label: 'Công nghệ' },
      { id: 2, slug: 'giao-duc', label: 'giáo dục' },
    ],
    category: 1,
    status: 'To be approved',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '1', name: 'Ngan' },
    publishedDate: '2011-09-30T14:48:00.000Z',
    numberOfView: 3,
  },
  {
    title: 'Điện thoại Samsung Galaxy M51',
    slug: 'samsung-galaxy-m51',
    image: '/img/articles/samsung-galaxy-m51.jpg',
    thumbnail: '/img/articles/samsung-galaxy-m51',
    author: { id: 1, name: 'Broze' },
    excerpt:
      'Samsung lại tiếp tục cho ra mắt chiếc smartphone mới thuộc thế hệ Galaxy M với tên gọi là Samsung Galaxy M51. Thiết kế mới này tuy nằm trong phân khúc tầm trung nhưng được Samsung nâng cấp và cải tiến với camera góc siêu rộng, dung lượng pin siêu khủng cùng vẻ ngoài sang trọng và thời thượng.',
    content:
      'Ấn tượng ban đầu với màn hình của Galaxy M51 là kiểu màn hình Infinity-O rộng 6.7 inch. Kiểu thiết kế này đưa camera selfie thu gọn hơn chỉ bằng một hình tròn nhỏ cùng thiết kế màn hình tràn viền làm tăng khả năng hiển thị hình ảnh hơn. Ngoài ra, máy còn sở hữu công nghệ màn hình Super AMOLED Plus mang đến chất lượng hiển thị sắc nét, hình ảnh tươi tắn cho bạn tận hưởng các chương trình giải trí hấp dẫn, thưởng thức các bộ phim bom tấn, chơi những tựa game yêu thích vô cùng bắt mắt.',
    tag: [
      { id: 1, slug: 'cong-nghe', label: 'Công nghệ' },
      { id: 2, slug: 'giao-duc', label: 'giáo dục' },
    ],
    category: 2,
    status: 'To be approved',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '1', name: 'Ngan' },
    publishedDate: '2011-09-29T14:48:00.000Z',
    numberOfView: 3,
  },
  {
    title: 'Điện thoại Samsung Galaxy Note 20',
    slug: 'samsung-galaxy-note-21',
    image: '/img/articles/samsung-galaxy-note-20.jpg',
    thumbnail: '/articles/samsung-galaxy-note-20',
    author: { id: 1, name: 'Broze' },
    excerpt:
      'Tháng 8/2020, Samsung Galaxy Note 20 chính thức được lên kệ, với thiết kế camera trước nốt ruồi quen thuộc, cụm camera hình chữ nhật mới lạ cùng với việc sử dụng vi xử lý Exynos 990 cao cấp của chính Samsung chắc hẳn sẽ mang lại một trải nghiệm thú vị cùng hiệu năng mạnh mẽ.',
    content:
      'Điện thoại sở hữu thiết kế khung kim loại chắc chắn, mặt lưng nhựa bóng bẩy, kiểu dáng mạnh mẽ với những góc cạnh vuông vức nhưng vẫn mang lại cảm giác cầm nắm thoải mái..',
    tag: [
      { id: 1, slug: 'cong-nghe', label: 'Công nghệ' },
      { id: 2, slug: 'giao-duc', label: 'giáo dục' },
    ],
    category: 7,
    status: 'To be approved',
    creator: '1',
    datecreated: '2011-10-05T14:48:00.000Z',
    editer: { id: '1', name: 'Ngan' },
    publishedDate: '2011-09-28T14:48:00.000Z',
    numberOfView: 3,
  },
]
