module.exports = [
  {
    id: 1,
    slug: 'index',
    title: 'Wata Solutions',
    description:
      'Our global team is focused on one thing: creating world-class mobile experiences that match the promises done and delivering them on time. "ALL ABOUT THE CUSTOMER"',
    image: 'http://localhost:3000/img/banner.jpg',
    sections: [
      {
        background: '/img/banner.jpg',
        layout: '1/2l',
      },
      {
        type: 'expertise',
        numberOfItems: 3,
        layout: '3col',
        items: [
          {
            id: 1,
            title: 'Software Solutions',
            description: `<ul>
            <li>Customer Software Development</li>
            <li>Enhancement & Maintenance support</li>
            <li>Software Testing</li>
          </ul>`,
            icon: '/img/expertise/solutions.svg',
          },
          {
            id: 2,
            title: 'Investment',
            description: `<ul>
            <li>Customer Software Development</li>
            <li>Enhancement & Maintenance support</li>
            <li>Software Testing</li>
          </ul>`,
            icon: '/img/expertise/investment.svg',
          },
          {
            id: 3,
            title: 'Software Product',
            description: `<ul>
            <li>Customer Software Development</li>
            <li>Enhancement & Maintenance support</li>
            <li>Software Testing</li>
          </ul>`,
            icon: '/img/expertise/product.svg',
          },
        ],
      },
      {
        title: 'About Us',
        description: `<p>Established in 2016, WATA solutions has developed rapidly and has become a pioneer in providing high quality software solutions in various fields and serving many domestic and foreign customers such as North America, Singapore, Korea and Japan.</p>
        <p>Our beliefs and core competencies in user-forcused design across multidisciplinary areas allow for seamless consistency and a greater focus on the details that matter.</p>`,
        type: 'literal',
        layout: '1/2l',
        image: '/img/aboutus/aboutus.jpg',
      },
      {
        background: '/img/stats/stats.jpg',
        items: [
          {
            id: 1,
            title: '150',
            icon: '/img/stats/investments.svg',
            description: 'Investments',
          },
          {
            id: 2,
            title: '300',
            icon: '/img/stats/projects.svg',
            description: 'Projects',
          },
          {
            id: 3,
            title: '3 Milion',
            icon: '/img/stats/hoursofcode.svg',
            description: 'Hours of Code',
          },
        ],
      },
      {
        title: 'Why Wata',
        items: [
          {
            id: 1,
            title: 'Quality focus',
            icon: '/img/whywata/quality.svg',
            description:
              'Quality is our pride and is the top criterion of whatever we do',
          },
          {
            id: 2,
            title: 'Reasonable and competitive price',
            icon: '/img/whywata/price.svg',
            description:
              'You will be satisfied with the price you paid for the quality you receive',
          },
          {
            id: 3,
            title: 'Quick ramp up with experience resource',
            icon: '/img/whywata/resource.svg',
            description:
              'Our enthusiastic and experienced resources are launchers for your successsuccess',
          },
          {
            id: 4,
            title: 'Adapting with clients’/partners’ working model & processes',
            icon: '/img/whywata/process.svg',
            description:
              'We are always flexible to optimize time and resources',
          },
        ],
      },
      {
        items: [
          {
            id: 1,
            icon: '/img/values/collaboration.svg',
            title: 'Well collaboration',
            description: `<ul>
            <li>Responsiveness to customers’ services</li>
            <li>Proactively provide valuable feedbacks/inputs from hands-on experience of emerge technology</li>
            <li>Daily interaction over emails, instant messages/audio/video conference</li>
          </ul>`,
          },
          {
            id: 2,
            icon: '/img/values/protection.svg',
            title: 'Infrastructure & IP Protection',
            description: `<ul>
            <li>Hosting and repository are on cloud</li>
            <li>Following ISO27001 best practices</li>
            <li>Well-defined and implemented network/data security system</li>
          </ul>`,
          },
          {
            id: 3,
            icon: '/img/values/flexible.svg',
            title: 'Flexible working process & implementation',
            description: `<ul>
            <li>Well-established strategy to retain and develop people</li>
            <li>Focus training and learning environment</li>
            <li>Good evaluation and rewarding system</li>
            <li>Good employees care</li>
          </ul>`,
          },
        ],
      },
      {
        title: 'What client says',
        items: [
          {
            id: 1,
            image: '/img/testimonials/person.jpg',
            icon: '/img/testimonials/sample.svg',
            title: 'Mr. Adobe Draw IO',
            meta: 'Fpt telecom',
            description: `<p>We adopt an integrated approach to digital solutions and collaborative processes. By empowering our people, we elevate talent, lead ingenuity and guide others towards innovation.We adopt an integrated approach to digital solutions and collaborative processes. By empowering our people, we elevate talent, lead ingenuity and guide others towards innovation.</p>`,
          },
          {
            id: 2,
            image: '/img/testimonials/person.jpg',
            icon: '/img/testimonials/sample.svg',
            title: 'Mr. Photoshop',
            meta: 'The Gioi Di Dong corp',
            description: `<p>Whether you're new to computer science or an experienced coder, there’s something for you here in Google’s Tech Dev Guide.</p>`,
          },
        ],
      },
      {
        layout: '3-col',
        title: 'Tech blog',
      },
    ],
  },
  {
    id: 2,
    slug: 'about',
    title: 'About Wata',
    description:
      'WATA is a team of experienced professionals, who excellent at solving various business challenges. From full-stack app development and DevOps services to Big Data analytics — we provide end-to-end solutions and support for your business.',
    image: '/img/banner.jpg',
    sections: [
      {
        background: '/img/banner.jpg',
        layout: '1/2l',
      },
      {
        title: 'About Us',
        description: `<p>Wata is a team of experienced professionals, who excellent at solving various business challenges.</p>
          <p>From full-stack app development and DevOps services to Big Data analytics — 
          we provide end-to-end solutions and support for your business.</p>
        `,
        layout: '1/3l',
      },
      {
        title: 'Our mission & vision',
        description: `<p>We believe that every business, striving to introduce a great idea of product or service to the world, must have every technical capability to do so.
          Our mission lies in providing high-quality and affordable cloud consulting and management services to help startups and companies of any size accomplish projects of any scope.
          We help the businesses leverage their strong sides and assist them with delivering great products and services to ensure a positive end-user experience.
        </p>`,
        background: '/img/mission.jpg',
        layout: '1/2r',
      },
      {
        title: 'Core Values',
        image: '/img/corevalues/corevalues.svg',
        items: [
          {
            id: 1,
            title: 'Innovation',
            icon: '/img/corevalues/innovation.svg',
            description:
              'Is constant in ideas, methods, and practices of the experienced employees at WATA always help customers improve efficiency, lead the trend, and feel satisfied.',
          },
          {
            id: 2,
            title: 'Commitment and Quality',
            icon: '/img/corevalues/commitment.svg',
            description:
              'Is our pride and is the guideline in all activities. Therefore, WATA has always gained the trust of customers during the past time. We commit to providing our customers the best quality in each product and service we provide.',
          },
          {
            id: 3,
            title: 'People',
            icon: '/img/corevalues/people.svg',
            description:
              'Is our pride and is the guideline in all activities. Therefore, WATA has always gained the trust of customers during the past time. We commit to providing our customers the best quality in each product and service we provide.',
          },
        ],
        layout: '1/2l',
      },
      {
        title: 'Management Team',
        items: [
          {
            id: 1,
            image: 'https://i.pravatar.cc/150?u=thien.dam@pravatar.com',
            title: 'Thien Dam',
            meta: 'C.E.O, Chair-man, Co-Founder',
            description: `
            <p>Thien has over 15 years of software development experience. He's dedicated to building agile development teams with the capabilities to tackle any project.</p>
            <p>He also holds a Bachelor's degree in Organizational Management and a Master's degree in Training and Development. He has spent the majority of his professional career creating, growing, and exiting start-ups in a variety of different verticals.</p>
          `,
          },
          {
            id: 2,
            image: 'https://i.pravatar.cc/150?u=trung.huynh@pravatar.com',
            title: 'Trung Huynh',
            meta: 'C.S.O, Co-Founder',
            description: `
            <p>Thien has over 15 years of software development experience. He's dedicated to building agile development teams with the capabilities to tackle any project.</p>
            <p>He also holds a Bachelor's degree in Organizational Management and a Master's degree in Training and Development. He has spent the majority of his professional career creating, growing, and exiting start-ups in a variety of different verticals.</p>
          `,
          },
          {
            id: 3,
            image: 'https://i.pravatar.cc/150?u=luc.nguyen@pravatar.com',
            title: 'Luc Nguyen',
            meta: 'C.O.O, Co-Founder',
            description: `
            <p>Thien has over 15 years of software development experience. He's dedicated to building agile development teams with the capabilities to tackle any project.</p>
            <p>He also holds a Bachelor's degree in Organizational Management and a Master's degree in Training and Development. He has spent the majority of his professional career creating, growing, and exiting start-ups in a variety of different verticals.</p>
          `,
          },
        ],
      },
      {
        title: 'recognitions & awards',
        items: [
          {
            id: 1,
            image: '/img/award/award1.jpg',
            title: 'MCSA',
          },
          {
            id: 2,
            image: '/img/award/award2.jpg',
            title: 'MCSD',
          },
          {
            id: 3,
            image: '/img/award/award3.jpg',
            title: 'PSM',
          },
          {
            id: 4,
            image: '/img/award/award4.jpg',
            title: 'VietNamIT',
          },
          {
            id: 5,
            image: '/img/award/award5.jpg',
            title: 'MCP Web Application',
          },
          {
            id: 6,
            image: '/img/award/award6.jpg',
            title: 'MCP App Builder',
          },
          {
            id: 7,
            image: '/img/award/award7.jpg',
            title: 'PSM',
          },
          {
            id: 8,
            image: '/img/award/award8.jpg',
            title: 'DN success the first',
          },
        ],
      },
      {
        title: 'Client & Partners',
        background: '/img/clientawards/bg_clientaward.jpg',
        items: [
          {
            id: 1,
            image: '/img/clientawards/clientaward1.jpg',
            title: 'VK LINK',
            linkTo: 'https://www.vklink.vn/?lang=vi',
          },
          {
            id: 2,
            image: '/img/clientawards/clientaward2.jpg',
            title: 'mobibletechRX',
            linkTo: 'https://www.mobiletechrx.com/',
          },
          {
            id: 3,
            image: '/img/clientawards/clientaward3.jpg',
            title: 'BOHEMIAN',
            linkTo: 'https://www.vklink.vn/?lang=vi',
          },
          {
            id: 4,
            image: '/img/clientawards/clientaward4.jpg',
            title: 'Twentyseven Global',
            linkTo: 'https://www.27global.com/',
          },
          {
            id: 5,
            image: '/img/clientawards/clientaward5.jpg',
            title: 'DS decision science',
            linkTo: 'https://www.decision-science.com/',
          },
          {
            id: 6,
            image: '/img/clientawards/clientaward6.jpg',
            title: 'MIRAE ASSET Finance Company',
            linkTo: 'https://mafc.com.vn/',
          },
          {
            id: 7,
            image: '/img/clientawards/clientaward7.jpg',
            title: 'KYOWON',
            linkTo: 'http://theorm.vn/theorm/main.do',
          },
          {
            id: 8,
            image: '/img/clientawards/clientaward8.jpg',
            title: 'CHANGI Airport Singapore',
            linkTo: 'https://www.changiairport.com/',
          },
          {
            id: 9,
            image: '/img/clientawards/clientaward9.jpg',
            title: "KOMAT'SU",
            linkTo: 'https://home.komatsu/en/',
          },
          {
            id: 10,
            image: '/img/clientawards/clientaward10.jpg',
            title: 'Roadrunner Transportation Systems',
            linkTo: 'https://www.shiproadrunnerfreight.com/',
          },
          {
            id: 11,
            image: '/img/clientawards/clientaward11.jpg',
            title: 'Far East Organization',
            linkTo: 'https://www.fareast.com.sg/',
          },
          {
            id: 12,
            image: '/img/clientawards/clientaward12.jpg',
            title: 'CREATIVE PLANNING',
            linkTo: 'https://creativeplanning.com/',
          },
          {
            id: 13,
            image: '/img/clientawards/clientaward13.jpg',
            title: 'GRAIN CRAFT',
            linkTo: 'https://www.graincraft.com/',
          },
          {
            id: 14,
            image: '/img/clientawards/clientaward14.jpg',
            title: 'Great Eastern Life is Greate',
            linkTo: 'https://www.greateasternlife.com/sg/en/index.html',
          },
          {
            id: 15,
            image: '/img/clientawards/clientaward15.jpg',
            title: 'Star Fate',
            linkTo:
              'https://www.nshop.com.vn/products/fate-extella-the-umbral-star-switch#gref',
          },
        ],
      },
    ],
  },
  {
    id: 3,
    slug: 'blog',
    title: 'Tech Blog',
    description:
      'Sharing idea and knowledge about latest technologies and principles',
  },
  {
    id: 4,
    slug: 'careers',
    title: 'Careers',
    description:
      'We are a collective of developers, designers, strategists and everything. We bring together in one fantastical environment and grow together',
    image: '/img/careers-bg.jpg',
    sections: [
      {
        background: '/img/careers-bg.jpg',
        layout: '1/2l',
        title: "You will happy when you're at Wata",
        description: `<p>We are a collective of developers, designers, strategists and everything.
          We bring together in one fantastical environment and grow together. We are those who:</p>
          <ul>
            <li>Dynamic, creative</li>
            <li>Sharing Together</li>
            <li>Passion for work</li>
            <li>Work is responsible</li>
            <li>Discover new things</li>
            <li>Like traveling</li>
            <li>Love football, talk comedy</li>
          </ul>
        `,
      },
      {
        title: 'Why will you love working here?',
        image: '/img/careers.jpg',
        description: `<ul>
          <li>Attractive signing bonus and competitive salary with many benefit</li>
          <li>5 working days (Monday to Friday)</li>
          <li>Friendly, professional, and open working environment.</li>
          <li>Premium Health Insurance package</li>
          <li>Guaranteed permanent employment and career development.</li>
          <li>Skills up plan with Tech talk, soft skill talk, English club,
            working process training</li>
          <li>Opportunities to gain hands-on experience in cutting-edge technologies</li>
          <li>Company trip yearly</li>
        </ul>`,
      },
      {
        title: 'Current Offers',
        description:
          'We offer interesting opportunities and attactive packages for your talents',
      },
    ],
  },
  {
    id: 5,
    title: 'Contact Us',
    slug: 'contact',
    description:
      'If you have any ideas and projects but have not done, let us help you develop it perfectly. Contact us immediately you will be pleased about it.',
    sections: [
      {
        background: '/img/contact.jpg',
        layout: '1/2l',
        type: 'banner',
      },
      {
        type: 'locations',
        items: [
          {
            id: 1,
            title: 'Head Office',
            description:
              'Hai Au Building, 39B Truong Son, Ward 4, Tan Binh Dist., HCMC, Vietnam',
          },
          {
            id: 2,
            title: 'Branch Office',
            description:
              'SBI Building, Quang Trung Software City, To Ky Street, Dist 12, HCMC, Vietnam',
          },
          {
            id: 3,
            title: 'Branch Office in Da Nang',
            description:
              '1120 Ngo Quyen, An Hai Ward, Son Tra Dist, DaNang , Vietnam',
          },
        ],
      },
      {
        type: 'contact',
        title: 'Contact',
        description:
          'If yiou have any ideas and projects but have not done, let us help you develop it perfectly.<br />Contact us immediately you will be pleased about it.',
      },
      {
        type: 'map',
      },
    ],
  },
  {
    id: 6,
    slug: 'services',
    title: 'Services',
    description:
      'WATA Corp specializes in creating custom software solutions for various industries - spanning different levels and requirements.',
    sections: [
      {
        title: 'Software Solution Services',
        description: `<p>WATA Corp specializes in creating custom software solutions for various industries - spanning different levels and requirements.</p>`,
        background: '/img/services/banner/banner.jpg',
        layout: '1/2l',
      },
      {
        background: '/img/services/software/software.jpg',
        items: [
          {
            id: 1,
            title: 'Customer Software Development',
            description:
              'On a wide range of platforms, from full-stack (Java, .NET, Java Script, PHP & Python) to mobile native application',
            image: 'img/services/software/softwareDev.svg',
          },
          {
            id: 2,
            title: 'Software Enhancement',
            description:
              'Bases on change and upgrade that increases software capabilities beyond original client specifications',
            image: 'img/services/software/softwareEnhancement.svg',
          },
          {
            id: 3,
            title: 'Software Maintenance Support',
            description:
              'Both adaptive maintenance and perfective maintenance for your software stable and improve performance',
            image: 'img/services/software/softwareMaintenance.svg',
          },
          {
            id: 4,
            title: 'Software testing',
            description:
              'Finding software bugs, provide an objective and exact view for quality assurance, and understand the risks of software implementation',
            image: '/img/services/software/softwareTest.svg',
          },
        ],
      },
      {
        title: 'Software Products',
        description: `<p>A combination of software technologies with innovation and creative ideas to serve and make life better</p>`,
        layout: '1/2r',
        image: '/img/services/software/products.svg',
      },
      {
        title: 'Investment',
        description: `<p>We believe that a lot of creative and wonderful ideas around us couldn't actualize due to lack of investment. So we're ready to collaborate and invest in potential projects</p>`,
        layout: '1/2l',
        image: '/img/services/software/investment.svg',
      },
      {
        title: 'Focused Domain',
        items: [
          {
            id: 1,
            title: 'blockchain',
            icon: '/img/services/focusedDomain/blockchain.svg',
            description: '',
          },
          {
            id: 2,
            title: 'fintech',
            icon: '/img/services/focusedDomain/fintech.svg',
            description: '',
          },
          {
            id: 3,
            title: 'ecommerce',
            icon: '/img/services/focusedDomain/ecommerce.svg',
            description: '',
          },
          {
            id: 4,
            title: 'estate',
            icon: '/img/services/focusedDomain/estate.svg',
            description: '',
          },
          {
            id: 5,
            title: 'ensurance',
            icon: '/img/services/focusedDomain/insurance.svg',
            description: '',
          },
          {
            id: 6,
            title: 'ai-ar',
            icon: '/img/services/focusedDomain/aiAr.svg',
            description: '',
          },
          {
            id: 7,
            title: 'location-based',
            icon: '/img/services/focusedDomain/locationBased.svg',
            description: '',
          },
          {
            id: 8,
            title: 'social network',
            icon: '/img/services/focusedDomain/socialNet.svg',
            description: '',
          },
          {
            id: 9,
            title: 'web content management',
            icon: '/img/services/focusedDomain/webContentManager.svg',
            description: '',
          },
          {
            id: 10,
            title: 'entreprise content management',
            icon: '/img/services/focusedDomain/enterContentManagement.svg',
            description: '',
          },
          {
            id: 11,
            title: 'logistics & supply chain management',
            icon: '/img/services/focusedDomain/logisticsSupplyChainMan.svg',
            description: '',
          },
        ],
      },
      {
        title: 'Focused Technologies',
        items: [
          {
            id: 1,
            title: 'PHP',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/2/27/PHP-logo.svg',
          },
          {
            id: 2,
            title: 'AWS',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/9/93/Amazon_Web_Services_Logo.svg',
          },
          {
            id: 3,
            title: 'iOS',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/c/ca/IOS_logo.svg',
          },
          {
            id: 4,
            title: 'Microservice',
            icon: '/img/services/focusedTech/microService.jpg',
          },
          {
            id: 5,
            title: 'Azure',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/a/a8/Microsoft_Azure_Logo.svg',
          },
          {
            id: 6,
            title: 'Flutter',
            icon: 'https://logowiki.net/uploads/logo/f/flutter.svg',
          },
          {
            id: 7,
            title: 'C/C++',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg',
          },
          {
            id: 8,
            title: 'DevOps',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/0/05/Devops-toolchain.svg',
          },
          {
            id: 9,
            title: 'Node.js',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/d/d9/Node.js_logo.svg',
          },
          {
            id: 10,
            title: '',
            icon:
              'https://upload.wikimedia.org/wikipedia/it/2/2e/Java_Logo.svg',
            description: '',
          },
          {
            id: 11,
            title: 'Android',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/d/d7/Android_robot.svg',
          },
          {
            id: 12,
            title: 'Angular',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/c/cf/Angular_full_color_logo.svg',
            description: '',
          },
          {
            id: 13,
            title: 'React',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg',
          },
          {
            id: 14,
            title: '.NET',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/a/a3/.NET_Logo.svg',
            description: '',
          },
          {
            id: 15,
            title: 'Python',
            icon:
              'https://upload.wikimedia.org/wikipedia/commons/f/f8/Python_logo_and_wordmark.svg',
          },
        ],
      },
    ],
  },
  {
    id: 7,
    slug: 'works',
    title: 'Our Work',
    description: '',
    image: '/img/works/banner.jpg',
    sections: [
      {
        background: '/img/works/banner.jpg',
        layout: '1/2l',
      },
      {
        layout: '3-col',
        title: 'Featured Projects',
      },
      {
        title: 'Working Process',
        description: `<p>We follow Agile/Scrum and the best practices of CMMI (Capability Maturity Model® Integration).
          We apply new technologies and project management tools as well as developing to raise efficiency and quality such as:
          Jira, Azure TFS, Github, Gitlab, BitBucket, Sonarqube, SVN DevOps, CI/CD, Unit Testing</p>`,
        layout: '1/2l',
        image: '/img/works/workingprocess.jpg',
      },
    ],
  },
]
