module.exports = [
  {
    id: 1,
    slug: 'job-1',
    title:
      'All in the Flex: YouTube Gets New Optimization for the Galaxy Z Flip',
    status: 'NEW',
    postedDate: '2021-05-13T00:00:00Z',
    content: `
      <h3>Job Description</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Other Skills</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Your Skills and Experience</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
    `,
  },
  {
    id: 2,
    slug: 'job-2',
    title:
      'All in the Flex: YouTube Gets New Optimization for the Galaxy Z Flip',
    status: 'NEW',
    postedDate: '2021-05-13T00:00:00Z',
    content: `
      <h3>Job Description</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Other Skills</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Your Skills and Experience</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
    `,
  },
  {
    id: 3,
    slug: 'job-3',
    title:
      'All in the Flex: YouTube Gets New Optimization for the Galaxy Z Flip',
    postedDate: '2021-05-13T00:00:00Z',
    content: `
      <h3>Job Description</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Other Skills</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Your Skills and Experience</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
    `,
  },
  {
    id: 4,
    slug: 'job-4',
    title:
      'All in the Flex: YouTube Gets New Optimization for the Galaxy Z Flip',
    postedDate: '2021-05-13T00:00:00Z',
    content: `
      <h3>Job Description</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Other Skills</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Your Skills and Experience</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
    `,
  },
  {
    id: 5,
    slug: 'job-5',
    title:
      'All in the Flex: YouTube Gets New Optimization for the Galaxy Z Flip',
    postedDate: '2021-05-13T00:00:00Z',
    content: `
      <h3>Job Description</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Other Skills</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Your Skills and Experience</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
    `,
  },
  {
    id: 6,
    slug: 'job-6',
    title:
      'All in the Flex: YouTube Gets New Optimization for the Galaxy Z Flip',
    postedDate: '2021-05-13T00:00:00Z',
    content: `
      <h3>Job Description</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Other Skills</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
      <h3>Your Skills and Experience</h3>
      <ul>
        <li>Operate Cloud services hosted in AWS, GCP, IBM Cloud, Azure.</li>
        <li>Automate all operations, treat all infrastructure as code.</li>
        <li>Respond to alerts, follow incident management procedures (within business hours).</li>
        <li>Measure everything, operate based on Service Level Indicators and Objectives.</li>
        <li>Migrate complex workloads from on-premises to Cloud.</li>
        <li>Develop delivery pipelines, handle configuration management.</li>
        <li>Have a focus on compliance and best practices.</li>
      </ul>
    `,
  },
]
