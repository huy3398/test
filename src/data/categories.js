module.exports = [
  {
    id: 1,
    slug: 'cloud',
    name: 'Cloud transformation',
  },
  {
    id: 2,
    slug: 'mobile',
    name: 'Mobile',
  },
  {
    id: 3,
    slug: 'devops',
    name: 'DevOps',
  },
  {
    id: 4,
    slug: 'news',
    name: 'Colorado News',
  },
  {
    id: 5,
    slug: 'software',
    name: 'Software Development',
  },
  {
    id: 6,
    slug: 'spotlight',
    name: 'Staff Spotlight',
  },
  {
    id: 7,
    slug: 'reliability',
    name: 'Site Reliability',
  },
]
