import { useMediaQuery } from 'react-responsive'
import { DefaultLayout } from '../layouts'
import { api } from '../utils/url'
import Banner from '../components/banner/Banner'
import BannerMobile from '../components/banner/BannerMobile'
import SoftwaresSection from '../components/software/SoftwareSection'
import DomainsSection from '../components/domains/DomainsSection'
import TechnologiesSection from '../components/technologies/TechnologiesSection'
import LiteralSection from '../components/literal/LiteralSection'
import LiteralMobileSection from '../components/literal/LiteralMobileSection'
import PageMeta from '../components/page-meta/PageMeta'
import bannerStyles from '../components/banner/banner.module.scss'
import sectionStyles from '../components/section/section.module.scss'
import SoftwareMobileSection from '../components/software/SoftwareMobileSection'

const Services = ({ dataRes }) => {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  })
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  if (isTabletOrMobile) {
    return (
      <DefaultLayout>
        <PageMeta data={dataRes} />
        <BannerMobile
          {...dataRes?.sections[0]}
          background='/img/services/banner/banner2.png'
        />
        <SoftwareMobileSection {...dataRes?.sections[5]} />
        <LiteralMobileSection
          {...dataRes.sections[1]}
          valign='middle'
          className={sectionStyles.serviceLiteral}
          layout='1/2r'
        />
        <LiteralMobileSection
          {...dataRes.sections[2]}
          valign='middle'
          className={sectionStyles.serviceLiteral}
          layout='1/2l'
        />
        <DomainsSection
          {...dataRes?.sections[3]}
          isDesktopOrLaptop={isDesktopOrLaptop}
          isTabletOrMobile={isTabletOrMobile}
        />
        <TechnologiesSection
          {...dataRes.sections[4]}
          isDesktopOrLaptop={isDesktopOrLaptop}
          isTabletOrMobile={isTabletOrMobile}
        />
      </DefaultLayout>
    )
  }

  return (
    <DefaultLayout>
      <PageMeta data={dataRes} />
      <Banner {...dataRes?.sections[0]} className={bannerStyles.leftGradient} />
      <SoftwaresSection {...dataRes?.sections[5]} />
      <LiteralSection
        {...dataRes.sections[1]}
        valign='middle'
        className={sectionStyles.serviceLiteral}
        containerClassName='px-6'
        layout='1/2r'
      />
      <LiteralSection
        {...dataRes.sections[2]}
        valign='middle'
        className={sectionStyles.serviceLiteral}
        containerClassName='px-6'
        layout='1/2l'
      />
      <DomainsSection
        {...dataRes?.sections[3]}
        isDesktopOrLaptop={isDesktopOrLaptop}
        isTabletOrMobile={isTabletOrMobile}
      />
      <TechnologiesSection
        {...dataRes.sections[4]}
        isDesktopOrLaptop={isDesktopOrLaptop}
        isTabletOrMobile={isTabletOrMobile}
      />
    </DefaultLayout>
  )
}

export default Services

export async function getServerSideProps() {
  const dataRes = await fetch(api('page/services', {}, false)).then(response =>
    response.json(),
  )

  return {
    props: { dataRes },
  }
}
