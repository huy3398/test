import { useMediaQuery } from 'react-responsive'
import Banner from '../components/banner/Banner'
import Locations from '../components/locations/Locations'
import PageMeta from '../components/page-meta/PageMeta'
import { DefaultLayout } from '../layouts'
import { api } from '../utils/url'
import ContactSection from '../components/contact/ContactSection'
import GoogleMap from '../components/map/GoogleMap'
import BannerMobile from '../components/banner/BannerMobile'

const Contact = ({ data }) => {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  })
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  return (
    <DefaultLayout>
      <PageMeta data={data} />

      {isDesktopOrLaptop && (
        <>
          <Banner {...data.sections[0]} />
        </>
      )}

      {isTabletOrMobile && (
        <>
          <BannerMobile {...data.sections[0]} />
        </>
      )}

      <Locations {...data.sections[1]} isTabletOrMobile={isTabletOrMobile} />
      <ContactSection {...data.sections[2]} />
      <GoogleMap />
    </DefaultLayout>
  )
}

export default Contact

export async function getServerSideProps() {
  const data = await fetch(api('pages/contact')).then(response =>
    response.json(),
  )
  return {
    props: { data }, // will be passed to the page component as props
  }
}
