import { BlogLayout } from '../../../layouts'
import { api } from '../../../utils/url'
import Section from '../../../components/section'
import ContentList from '../../../components/content-list/ContentList'
import PageMeta from '../../../components/page-meta/PageMeta'
import { pageTypes } from '../../../components/page-meta/jsonLd'

const Category = ({ articles, category }) => {
  const meta = {
    ...category,
    title: category.name,
    description: `Tech Blog Category: ${category.name}`,
  }
  return (
    <BlogLayout>
      <PageMeta data={meta} type={pageTypes.CATEGORY} />
      <Section title={category.name} sidebar className='my-5'>
        {articles?.data.length > 0 && (
          <ContentList
            data={articles?.data || []}
            columns={2}
            baseUrl='/blog'
          />
        )}
      </Section>
    </BlogLayout>
  )
}

export default Category

export async function getServerSideProps(context) {
  const { slug } = context.query

  const query = {
    limit: 5,
    filter: { category: slug },
  }

  const [articles, category] = await Promise.all([
  
    fetch(api(`articles/search?category-slug=${slug}`, {},false)).then(response => response.json()),
    fetch(api(`article-category/${slug}`, {}, false)).then(response => response.json()),
    // fetch(api('articles/search', query, false)).then(response => response.json()),
    // fetch(api(`article-category/${slug}`, {}, false)).then(response => response.json()),
  ])
  return {
    props: { articles, category },
  }
}
