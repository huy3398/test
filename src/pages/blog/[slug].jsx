import { ResponsiveEmbed } from 'react-bootstrap'
import { BlogLayout } from '../../layouts'
import { api } from '../../utils/url'
import HtmlContent from '../../components/html/HtmlContent'
import { readingTime, relative } from '../../utils/datetime'
import PageMeta from '../../components/page-meta/PageMeta'
import { pageTypes } from '../../components/page-meta/jsonLd'

const BlogArticle = ({ article }) => {
  return (
    <BlogLayout>
      <PageMeta data={article} type={pageTypes.BLOG} />
      <article className='my-5'>
        <header className='mb-4'>
          <ResponsiveEmbed aspectRatio='16by9' className='mb-4'>
            <img
              src={article.image}
              alt={article.title}
              className='d-block w-100 object-fit-cover rounded-lg'
            />
          </ResponsiveEmbed>
          <h1 className='font-weight-bold mb-3 h3'>{article.title}</h1>
          <p className='text-muted'>
            <span className='text-blue-dark'>By {article.author.name}</span>{' '}
            &bull;
            <time dateTime={article.publishedDate}>
              {relative(new Date(article.publishedDate))}
            </time>
          </p>
        </header>
        <section className='lead mb-4'>
          <HtmlContent>{article.excerpt}</HtmlContent>
        </section>
        <section>
          <HtmlContent>{article.content}</HtmlContent>
        </section>
      </article>
    </BlogLayout>
  )
}

export default BlogArticle

export async function getServerSideProps(context) {
  const { slug } = context.params
  const article = await fetch(api(`article/${slug}`, {}, false)).then(
    response => response.json(),
  )
  return {
    props: { article },
  }
}
