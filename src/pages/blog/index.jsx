import { useRouter } from 'next/router'
import { BlogLayout } from '../../layouts'
import { api } from '../../utils/url'
import Section from '../../components/section'
import ContentList from '../../components/content-list/ContentList'
import PageMeta from '../../components/page-meta/PageMeta'
import Pagination from '../../components/pagination/Pagination'
import { getTotalPages } from '../../utils/number'

const pageSize = 5

const Blog = ({ data = {}, articles = [], searchQuery }) => {
  const { query } = useRouter()
  const { page = 1 } = query

  const pageCount = getTotalPages(articles?.total, pageSize)

  const meta = { ...data }
  if (searchQuery) {
    meta.title = `Search: ${searchQuery}`
    meta.description = `Search blog article for keyword: ${searchQuery}`
  }
  if (parseInt(page, 10) > 1) {
    meta.title = `${meta.title}. Page ${page}`
    meta.description = `${meta.title} (Page ${page})`
  }

  return (
    <BlogLayout>
      <PageMeta data={meta} page={parseInt(page, 10)} />
      {!searchQuery && parseInt(page, 10) === 1 ? (
        <>
          <Section className='border-bottom my-5'>
            {articles?.data?.length > 0 && (
              <ContentList
                data={[articles.data[0]]}
                columns={1}
                excerpt
                baseUrl='/blog'
              />
            )}
          </Section>
          <Section title='Latest' sidebar className='my-5'>
            {articles?.data?.length > 1 && (
              <ContentList
                data={articles.data.filter((_, index) => index > 0)}
                columns={2}
                baseUrl='/blog'
                rowCol6
              />
            )}
          </Section>
        </>
      ) : (
        <Section sidebar className='my-5'>
          {articles?.data?.length > 0 && (
            <ContentList data={articles.data} columns={2} baseUrl='/blog' />
          )}
        </Section>
      )}
      {pageCount > 1 && (
        <Pagination pageCount={pageCount} currentPage={parseInt(page, 10)} />
      )}
    </BlogLayout>
  )
}

export default Blog

export async function getServerSideProps(context) {
  const { q: searchQuery = '', page = 1, size = 5 } = context.query

  const query1 = {
    skip: (page - 1) * size,
    size,
    search: searchQuery,
    page,
  }
  const [data, articles] = await Promise.all([
    fetch(api('page/blog', {}, false)).then(response => response.json()),
    fetch(api('articles/search', query1, false)).then(response =>
      response.json(),
    ),
  ])
  return {
    props: { data, articles, searchQuery },
  }
}
