import { sitemap } from '../../utils/url'

export default function handler(req, res) {
  const data = `<?xml version="1.0" encoding="UTF-8"?>
  <sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>
    <sitemap>
      <loc>${sitemap('pages')}</loc>
    </sitemap>
    <sitemap>
      <loc>${sitemap('articles')}</loc>
    </sitemap>
    <sitemap>
      <loc>${sitemap('categories')}</loc>
    </sitemap>
    <sitemap>
      <loc>${sitemap('works')}</loc>
    </sitemap>
    <sitemap>
      <loc>${sitemap('jobs')}</loc>
    </sitemap>
  </sitemapindex>`

  res.setHeader('Content-Type', 'application/xml')
  res.write(data)
  res.end()
}
