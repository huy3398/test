const paginate = (array, pageSize, pageNumber) => {
  return array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize)
}

export default function handler(req, res) {
  const mockupData = require('../../../data/projects')
  const pageNum = req?.query?.page || 1
  const total = mockupData?.length || 0
  const pageSize = 6
  const paginationData = paginate(mockupData, pageSize, pageNum)
  res.json({
    data: paginationData,
    total,
  })
}
