export default function handler(req, res) {
  const { slug } = req.query
  const articles = require('../../../data/articles')

  const article = articles.find(item => item.slug === slug)

  res.json(article)
}
