import get from 'lodash.get'

const searchBy = (text, keyword) =>
  text.toLowerCase().includes(keyword.toLowerCase())

export default function handler(req, res) {
  const {
    query: { limit, skip = 0, search, ...rest },
  } = req

  const category = get(rest, 'filter[category]', '')
  const articles = require('../../../data/articles')
  const categories = require('../../../data/categories')

  const totalData = articles
    .filter(el => el.status === 'Published')
    .filter(
      a =>
        !search ||
        searchBy(a.title, search) ||
        searchBy(a.excerpt, search) ||
        searchBy(a.content, search),
    )
    .map(a => {
      const currentCategory = categories.find(c => c.id === a.category)
      return { ...a, category: currentCategory }
    })
    .filter(a => !category || category === a.category.slug)

  const data = totalData
    .sort((a, b) => new Date(a.publishedDate) - new Date(b.publishedDate))
    .filter((_, index) => {
      return (
        !limit ||
        (index >= parseInt(skip, 10) &&
          index < parseInt(limit, 10) + parseInt(skip, 10))
      )
    })

  res.json({
    data,
    total: totalData.length,
  })
}
