export default function handler(req, res) {
  const categories = require('../../../data/categories')
  const articles = require('../../../data/articles')

  const data = categories
    .map(c => {
      const total =
        articles
          .filter(a => a.status === 'Published')
          .filter(a => a.category === c.id)?.length || 0
      return { ...c, total }
    })
    .filter(c => c.total)

  res.json(data)
}
