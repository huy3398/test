export default function handler(req, res) {
  const { slug } = req.query
  const categories = require('../../../data/categories')

  const category = categories.find(item => item.slug === slug)

  res.json(category)
}
