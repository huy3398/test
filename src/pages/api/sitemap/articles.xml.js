import getConfig from 'next/config'
import { absolute, api } from '../../../utils/url'

const { publicRuntimeConfig: config } = getConfig()

export default async function handler(req, res) {
  const articles = await fetch(api('articles'))
    .then(response => response.json())
    .then(json => json.data)
  const data = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
    ${articles
      .map(
        article => `<url>
        <loc>${absolute(`/blog/${article.slug}`)}</loc>
        <lastmod>${article.datecreated}</lastmod>
        <priority>1.0</priority>
        <news:news>
          <news:publication>
            <news:name>${config.company.name}</news:name>
            <news:language>vi</news:language>
          </news:publication>
          <news:publication_date>${
            article.publishedDate
          }</news:publication_date>
          <news:title>${article.title}</news:title>
        </news:news>
      </url>`,
      )
      .join('')}}
  </urlset>`

  res.setHeader('Content-Type', 'application/xml')
  res.write(data)
  res.end()
}
