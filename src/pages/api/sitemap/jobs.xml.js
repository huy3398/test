import { absolute, api } from '../../../utils/url'

export default async function handler(req, res) {
  const jobs = await fetch(api('jobs')).then(response => response.json())
  const data = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    ${jobs
      .map(
        job => `<url>
        <loc>${absolute(`/careers/${job.slug}`)}</loc>
        <lastmod>${job.datecreated}</lastmod>
        <priority>1.0</priority>
      </url>`,
      )
      .join('')}}
  </urlset>`

  res.setHeader('Content-Type', 'application/xml')
  res.write(data)
  res.end()
}
