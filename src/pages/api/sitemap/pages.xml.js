import routes from '../../../layouts/components/routes'
import { absolute } from '../../../utils/url'

export default function handler(req, res) {
  const data = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
      <loc>${absolute()}</loc>
      <lastmod></lastmod>
      <priority>1.0</priority>
    </url>
    ${routes
      .map(
        route => `
        <url>
          <loc>${absolute(route.href)}</loc>
          <lastmod></lastmod>
          <priority>0.8</priority>
        </url>
      `,
      )
      .join('')}
  </urlset>`

  res.setHeader('Content-Type', 'application/xml')
  res.write(data)
  res.end()
}
