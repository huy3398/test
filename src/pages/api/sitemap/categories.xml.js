import { absolute, api } from '../../../utils/url'

export default async function handler(req, res) {
  const categories = await fetch(api('categories')).then(response =>
    response.json(),
  )

  const data = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    ${categories
      .map(
        c => `<url>
        <loc>${absolute(`/blog/category/${c.slug}`)}</loc>
        <lastmod></lastmod>
        <priority>0.7</priority>
      </url>`,
      )
      .join('')}
  </urlset>`

  res.setHeader('Content-Type', 'application/xml')
  res.write(data)
  res.end()
}
