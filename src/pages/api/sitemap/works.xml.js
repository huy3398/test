import { absolute } from '../../../utils/url'

export default function handler(req, res) {
  const projects = require('../../../data/projects')
  const data = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    ${projects
      .map(
        proj => `<url>
        <loc>${absolute(`/careers/${proj.slug}`)}</loc>
        <lastmod>${proj.datecreated}</lastmod>
        <priority>0.7</priority>
      </url>`,
      )
      .join('')}}
  </urlset>`

  res.setHeader('Content-Type', 'application/xml')
  res.write(data)
  res.end()
}
