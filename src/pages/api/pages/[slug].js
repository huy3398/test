export default function handler(req, res) {
  const { slug = 'index' } = req.query

  const mockupData = require(`../../../data/pages`)
  const page = mockupData.find(p => p.slug === slug)

  res.json(page)
}
