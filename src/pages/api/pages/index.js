export default function handler(req, res) {
  const mockupData = require('../../../data/pages')

  res.json(mockupData)
}
