export default function handler(req, res) {
  const { slug } = req.query
  const mockupData = require('../../../data/jobs')

  const job = mockupData.find(item => item.slug === slug)

  res.json(job)
}
