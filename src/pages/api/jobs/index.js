export default function handler(req, res) {
  const mockupData = require('../../../data/jobs')

  res.json(mockupData)
}
