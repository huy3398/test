import Link from 'next/link'
import { Button, Container } from 'react-bootstrap'
import PageMeta from '../components/page-meta/PageMeta'

const NotFound = () => {
  return (
    <Container
      className='justify-content-center d-flex flex-column align-items-center py-4'
      style={{ minHeight: '100vh' }}
    >
      <PageMeta title='Page Not Found' />
      <img src='/img/404.svg' alt='404 - Not Found' height='300px' />
      <section className='text-center'>
        <h1 className='h2 mt-5 mb-4'>404 &ndash; Page Not Found</h1>
        <p className='mb-4'>
          The page you are looking for might have been removed
          <br /> had its name changed or is temporarily unavailable
        </p>
        <Link href='/' passHref>
          <Button as='a' variant='primary' size='lg' className='px-6 py-2'>
            Go to Homepage
          </Button>
        </Link>
      </section>
    </Container>
  )
}

export default NotFound
