import { useRouter } from 'next/router'
import Banner from '../../components/banner/Banner'
import PageMeta from '../../components/page-meta/PageMeta'
import { DefaultLayout } from '../../layouts'
import { api } from '../../utils/url'
import bannerStyles from '../../components/banner/banner.module.scss'
import WorkHereSection from '../../components/work-here/WorkHereSection'
import OffersSection from '../../components/offer/OffersSection'

const Careers = ({ data, jobs }) => {
  const { query } = useRouter()
  const { page = 1 } = query
  return (
    <DefaultLayout>
      <PageMeta data={data} page={parseInt(page, 10)} />

      <Banner {...data.sections[0]} className={bannerStyles.leftGradient} />
      <WorkHereSection {...data.sections[1]} />
      <OffersSection {...data.sections[2]} jobs={jobs} />
    </DefaultLayout>
  )
}

export default Careers

export async function getServerSideProps() {
  const [data, jobs] = await Promise.all([
    fetch(api('page/careers', {}, false)).then(response => response.json()),
    fetch(api('jobs', {}, false)).then(response => response.json()),
  ])

  return {
    props: { data, jobs  }, // will be passed to the page component as props
  }
}
