import { Col, Container, Row } from 'react-bootstrap'
import Banner from '../../components/banner/Banner'
import HtmlContent from '../../components/html/HtmlContent'
import ApplyButton from '../../components/offer/ApplyButton'
import OffersSidebar from '../../components/offer/OffersSidebar'
import { pageTypes } from '../../components/page-meta/jsonLd'
import PageMeta from '../../components/page-meta/PageMeta'
import WorkHereSection from '../../components/work-here/WorkHereSection'
import { DefaultLayout } from '../../layouts'
import { api } from '../../utils/url'
import styles from './job.module.scss'

const Job = ({ data, job, jobs }) => {
  return (
    <DefaultLayout>
      <PageMeta data={job} type={pageTypes.CAREER} />
      <Banner background={data.sections[0].background} />
      <Container fluid='xl'>
        <Row className='pt-6 pb-4'>
          <Col as='main' sm={{ span: 8 }}>
            <article>
              <header>
                <h1 className='font-weight-bold mb-5'>{job.title}</h1>
                <ApplyButton job={job} key={job.slug} />
              </header>
              <section className={styles.content}>
                <HtmlContent>{job.content}</HtmlContent>
              </section>
            </article>
          </Col>
          <Col as='aside' sm={{ span: 4 }}>
            <OffersSidebar title='New Jobs' jobs={jobs} />
          </Col>
        </Row>
      </Container>
      <WorkHereSection {...data.sections[1]} />
    </DefaultLayout>
  )
}

export default Job

export async function getServerSideProps(context) {
  const { slug } = context.params

  const [data, job, jobs] = await Promise.all([
    fetch(api('page/careers', {}, false)).then(response => response.json()),
    fetch(api(`job/${slug}`, {}, false)).then(response => response.json()),
    fetch(api(`jobs`, {}, false)).then(response => response.json()),
  ])

  return {
    props: { data, job, jobs }, // will be passed to the page component as props
  }
}
