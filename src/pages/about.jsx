import { useMediaQuery } from 'react-responsive'
import getConfig from 'next/config'
import { DefaultLayout } from '../layouts'
import Banner from '../components/banner/Banner'
import BannerMobile from '../components/banner/BannerMobile'
import AboutSection from '../components/about/AboutSection'
import TeamSection from '../components/team/TeamSection'
import CoreValuesSection from '../components/core-values/CoreValuesSection'
import { api } from '../utils/url'
import PageMeta from '../components/page-meta/PageMeta'
import LiteralSection from '../components/literal/LiteralSection'
import ClientsAwardSection from '../components/clients-awards/ClientsAwards'
import AwardSection from '../components/award/AwardSection'
import LiteralMobileSection from '../components/literal/LiteralMobileSection'
import AboutMobileSection from '../components/about/AboutMobileSection'
import TeamMobileSection from '../components/team/TeamMobileSection'

const { publicRuntimeConfig: config } = getConfig()

const About = ({ data }) => {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  })
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  return (
    <DefaultLayout>
      <PageMeta data={data} />
      {isTabletOrMobile && (
        <>
          <BannerMobile {...data.sections[0]} />
          <AboutMobileSection
            {...data.sections[1]}
            video
            image={config.media.about}
            imageClassName='shadowed'
            valign='middle'
          />
          <LiteralMobileSection
            {...data.sections[2]}
            contentClassName='bg-white shadow py-5 px-5'
          />
          <CoreValuesSection
            {...data.sections[3]}
            isTabletOrMobile={isTabletOrMobile}
          />
          <TeamMobileSection {...data.sections[4]} />
          {/* <AwardSection {...data.sections[5]} />
          <ClientsAwardSection {...data.sections[6]} /> */}
        </>
      )}

      {isDesktopOrLaptop && (
        <>
          <Banner {...data.sections[0]} />
          <AboutSection
            {...data.sections[1]}
            video
            image={config.media.about}
            imageClassName='shadowed'
            valign='middle'
          />
          <LiteralSection
            {...data.sections[2]}
            contentClassName='bg-white shadow py-5 px-5'
          />
          <CoreValuesSection {...data.sections[3]} />
          <TeamSection {...data.sections[4]} />
          <AwardSection {...data.sections[5]} />
          <ClientsAwardSection {...data.sections[6]} />
        </>
      )}
    </DefaultLayout>
  )
}

export default About

export async function getServerSideProps() {
  const data = await fetch(api('pages/about')).then(response => response.json())
  return {
    props: { data }, // will be passed to the page component as props
  }
}
