import { useMediaQuery } from 'react-responsive'
import { DefaultLayout } from '../../layouts'
import PageMeta from '../../components/page-meta/PageMeta'
import { api, apiV2 } from '../../utils/url'
import Banner from '../../components/banner/Banner'
import BannerMobile from '../../components/banner/BannerMobile'
import LiteralSection from '../../components/literal/LiteralSection'
import LiteralSectionMobile from '../../components/literal/LiteralSectionMobile'
import FeaturedProjectSection from '../../components/featured-project/FeaturedProjectSection'
import commonStyles from '../../components/section/section.module.scss'

const Works = ({ data, works, page, pageSize }) => {
  const meta = { ...data }

  if (parseInt(page, 10) > 1) {
    meta.title = `${meta.title}. Page ${page}`
    meta.description = `${meta.title} (Page ${page})`
  }

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  })
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  if (isTabletOrMobile) {
    return (
      <DefaultLayout>
        <PageMeta data={meta} page={parseInt(page, 10)} />
        <BannerMobile {...data.sections[0]} />
        <FeaturedProjectSection
          works={works}
          {...data.sections[1]}
          page={page}
          pageSize={pageSize}
        />
        <LiteralSectionMobile {...data.sections[2]} noGutters />
      </DefaultLayout>
    )
  }

  return (
    <DefaultLayout>
      <PageMeta data={meta} page={parseInt(page, 10)} />
      <Banner {...data.sections[0]} />
      <FeaturedProjectSection
        works={works}
        {...data.sections[1]}
        page={page}
        pageSize={pageSize}
        isDesktopOrLaptop={isDesktopOrLaptop}
      />
      <LiteralSection
        {...data.sections[2]}
        className={commonStyles.doubleBackground}
        contentClassName='bg-white shadow py-5 px-5'
        imageClassName='shadow'
        noGutters
      />
    </DefaultLayout>
  )
}

export default Works

export async function getServerSideProps({ query: { page = 1, size = 6 } }) {
  const [data, works] = await Promise.all([
    fetch(api('pages/works')).then(response => response.json()),
    fetch(apiV2(`works?page=${page}&size=${size}`, {}, false)).then(response =>
      response.json(),
    ),
  ])

  return {
    props: { data, works, page, pageSize: size },
  }
}
