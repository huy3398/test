import { Col, Container, ResponsiveEmbed, Row } from 'react-bootstrap'
import { useMediaQuery } from 'react-responsive'
import { DefaultLayout } from '../../layouts'
import Banner from '../../components/banner/Banner'
import { apiV2 } from '../../utils/url'
import Section from '../../components/section'
import ContentList from '../../components/content-list/ContentList'
import PageMeta from '../../components/page-meta/PageMeta'
import { pageTypes } from '../../components/page-meta/jsonLd'
import HtmlContent from '../../components/html/HtmlContent'
import { relative } from '../../utils/datetime'

const Work = ({ work, otherWorks }) => {
  const workList = otherWorks?.items || []

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  })

  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  return (
    <DefaultLayout>
      <PageMeta data={work} type={pageTypes.WORK} />
      {isDesktopOrLaptop && <Banner background='/img/work-detail/banner.jpg' />}
      <Container fluid='xl'>
        <Row className='pt-5 pb-4'>
          <Col as='main' sm={{ span: 8 }}>
            <article>
              <header className='mb-4'>
                <ResponsiveEmbed aspectRatio='16by9' className='mb-4'>
                  <img
                    src={work.image}
                    alt={work.title}
                    className='d-block w-100 object-fit-cover rounded-lg'
                  />
                </ResponsiveEmbed>
                <h1 className='font-weight-bold mb-3 h3'>{work.title}</h1>
                <p className='text-muted'>
                  Project Duration: {relative(new Date(work.createdDate))} -{' '}
                  {relative(new Date(work.endDate))}
                </p>
              </header>
              <section className='lead mb-4'>
                <HtmlContent>{work.excerpt}</HtmlContent>
              </section>
              <section>
                <HtmlContent>{work.content}</HtmlContent>
              </section>
            </article>
          </Col>
          <Col
            className={isTabletOrMobile ? 'mt-4' : ''}
            as='aside'
            sm={{ span: 4 }}
          >
            <Section
              sidebar
              title='Other case studies'
              isTabletOrMobile={isTabletOrMobile}
            >
              {workList.length && (
                <ContentList
                  data={[workList[0]]}
                  columns={1}
                  baseUrl='/works'
                  isTabletOrMobile={isTabletOrMobile}
                  projectItem
                />
              )}
            </Section>
            <Section sidebar isTabletOrMobile={isTabletOrMobile}>
              {workList.length > 1 && (
                <ContentList
                  data={workList.filter((_, index) => index > 0)}
                  columns={1}
                  horizontal
                  baseUrl='/works'
                  isTabletOrMobile={isTabletOrMobile}
                />
              )}
            </Section>
          </Col>
        </Row>
      </Container>
    </DefaultLayout>
  )
}

export default Work

export async function getServerSideProps(context) {
  const { slug } = context.params

  const work = await fetch(apiV2(`work/${slug}`)).then(response =>
    response.json(),
  )
  const otherWorks = await fetch(apiV2(`works?page=1&size=6`, {}, false)).then(
    response => response.json(),
  )

  return {
    props: { work, otherWorks },
  }
}
