import { useRouter } from 'next/router'
import { useMediaQuery } from 'react-responsive'
import { DefaultLayout } from '../layouts'
import Banner from '../components/banner/Banner'
import BannerMobile from '../components/banner/BannerMobile'
import ExpertiseSection from '../components/expertise/ExpertiseSection'
import AboutSection from '../components/about/AboutSection'
import StatsSection from '../components/stats/StatsSection'
import StatsMobileSection from '../components/stats/StatsMobileSection'
import WhyWataSection from '../components/whywata/WhyWataSection'
import WhyWataMobileSection from '../components/whywata/WhyWataMobileSection'
import ValuesSection from '../components/values/Values'
import TestimonialsSection from '../components/testimonials/TestimonialsSection'
import TestimonialsMobileSection from '../components/testimonials/TestimonialsMobileSection'
import BlogSection from '../components/blog/BlogSection'
import PageMeta from '../components/page-meta/PageMeta'
import { absolute, api } from '../utils/url'
import bannerStyles from '../components/banner/banner.module.scss'

function Home({ data, articles }) {
  const router = useRouter()
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  })
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  const goToPage = pathname => {
    router.push(pathname)
  }

  return (
    <DefaultLayout>
      <PageMeta data={data} url={absolute(router.pathname)} ogType='website' />
      {isTabletOrMobile && (
        <>
          <BannerMobile
            {...data.sections[0]}
            className={bannerStyles.bottomGradient}
          />
          <ExpertiseSection
            {...data.sections[1]}
            isTabletOrMobile={isTabletOrMobile}
          />
          <AboutSection {...data.sections[2]} imageClassName='bordered' />
          <StatsMobileSection {...data.sections[3]} />
          <WhyWataMobileSection {...data.sections[4]} />
          <ValuesSection
            {...data.sections[5]}
            isTabletOrMobile={isTabletOrMobile}
          />
          <TestimonialsMobileSection
            {...data.sections[6]}
            isTabletOrMobile={isTabletOrMobile}
          />
          <BlogSection
            {...data.sections[7]}
            articles={articles}
            isTabletOrMobile={isTabletOrMobile}
            rowCol4
          />
        </>
      )}

      {isDesktopOrLaptop && (
        <>
          <Banner
            {...data.sections[0]}
            className={bannerStyles.bottomGradient}
          />
          <ExpertiseSection {...data.sections[1]} />
          <AboutSection {...data.sections[2]} imageClassName='bordered' />
          <StatsSection {...data.sections[3]} />
          <WhyWataSection {...data.sections[4]} />
          <ValuesSection {...data.sections[5]} />
          <TestimonialsSection {...data.sections[6]} />
          <BlogSection {...data.sections[7]} articles={articles} rowCol4 />
          <div className='fixed-bottom mb-4 mr-4' style={{ left: 'auto' }}>
            <button
              onClick={() => goToPage('contact')}
              type='button'
              className='btn btn-secondary px-4 py-3 text-uppercase shadow'
            >
              Contact Us
            </button>
          </div>
        </>
      )}
    </DefaultLayout>
  )
}

export default Home

export async function getServerSideProps() {
  const [data, articles] = await Promise.all([
    fetch(api('pages/index')).then(response => response.json()),
    fetch(api('articles/search', { size: 3 }, false)).then(response =>
      response.json(),
    ),
  ])
  return {
    props: { data, articles }, // will be passed to the page component as props
  }
}
