import { Card, CardGroup } from 'react-bootstrap'
import Section from '../section'
import styles from './stats.module.scss'

const StatsSection = ({ items, background }) => {
  return (
    <Section className={styles.stats} background={background}>
      <CardGroup>
        {items.map(item => (
          <Card
            key={`stats-${item.id}`}
            bg='transparent'
            className='p-4 text-white border-0'
          >
            <Card.Body className='text-center'>
              <img src={item.icon} width='50px' alt={item.title} />
              <h3 className='h2 text-white mt-4'>{item.title}</h3>
              <span className='h5 text-white text-uppercase font-weight-medium'>
                {item.description}
              </span>
            </Card.Body>
          </Card>
        ))}
      </CardGroup>
    </Section>
  )
}

export default StatsSection
