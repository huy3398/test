import Section from '../section'
import styles from './stats.module.scss'

const StatsMobileSection = ({ items, background }) => {
  return (
    <Section className={styles.stats} background={background}>
      <div className='row'>
        {items.map(item => (
          <div
            key={`stats-${item.id}`}
            bg='transparent'
            className='p-4 text-white border-0 col-4'
          >
            <div className='text-center'>
              <img
                src={item.icon}
                width='30px'
                height='30px'
                alt={item.title}
              />
              <h3 className='h6 text-white mt-4'>{item.title}</h3>
              <span
                style={{ fontSize: '6px' }}
                className='text-white text-uppercase font-weight-medium'
              >
                {item.description}
              </span>
            </div>
          </div>
        ))}
      </div>
    </Section>
  )
}

export default StatsMobileSection
