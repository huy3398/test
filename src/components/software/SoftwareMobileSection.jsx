import clsx from 'clsx'
import { Col, Row } from 'react-bootstrap'
import Section from '../section'

const SoftwareMobileSection = ({ items }) => {
  return (
    <Section className={clsx('mb-5')}>
      <Row>
        {items.map(item => {
          return (
            <Col
              key={`software-${item.id}`}
              className='py-2 mx-1 my-4 align-middle text-center'
            >
              <img src={item.icon} alt={item.title} />
              <h3 className='h5 mx-2 mt-4 mb-3'>{item.title}</h3>
              <p className='mx-2 mb-0'>{item.description}</p>
            </Col>
          )
        })}
      </Row>
    </Section>
  )
}

export default SoftwareMobileSection
