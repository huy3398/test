import clsx from 'clsx'
import { Col, Row } from 'react-bootstrap'
import Section from '../section'
import styles from './software.module.scss'

const SoftwaresSection = ({ items, background }) => {
  return (
    <Section
      className={clsx(styles.softwareBg, 'mb-5')}
      background={background}
    >
      <Row>
        {items.map(item => {
          return (
            <Col
              key={`software-${item.id}`}
              className='px-4 py-5 mx-3 my-6 align-middle bg-white text-center shadow'
            >
              <img src={item.icon} alt={item.title} />
              <h3 className='h5 mx-2 mt-4 mb-3'>{item.title}</h3>
              <p className='mx-2 mb-0'>{item.description}</p>
            </Col>
          )
        })}
      </Row>
    </Section>
  )
}

export default SoftwaresSection
