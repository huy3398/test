import clsx from 'clsx'

const sizeMap = {
  default: '100px',
  lg: '180px',
}

const Avatar = ({ src, alt, size = 'default', className }) => {
  return (
    <img
      className={clsx('rounded-circle', className)}
      src={src}
      alt={alt}
      width={sizeMap[size]}
      height={sizeMap[size]}
    />
  )
}

export default Avatar
