import clsx from 'clsx'
import LiteralMobileSection from '../literal/LiteralMobileSection'
import styles from './about.module.scss'

const AboutMobileSection = ({ imageClassName, ...props }) => {
  return (
    <LiteralMobileSection
      imageClassName={clsx(styles[imageClassName], 'position-relative')}
      {...props}
    />
  )
}

export default AboutMobileSection
