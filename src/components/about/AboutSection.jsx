import clsx from 'clsx'
import LiteralSection from '../literal/LiteralSection'
import styles from './about.module.scss'

const AboutSection = ({ imageClassName, ...props }) => {
  return (
    <LiteralSection
      imageClassName={clsx(styles[imageClassName], 'position-relative')}
      {...props}
    />
  )
}

export default AboutSection
