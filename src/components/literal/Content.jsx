import clsx from 'clsx'
import { Col, Row } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'
import { SectionHeader } from '../section'
import { LiteralImage, LiteralVideo } from './LiteralImage'

const layoutRowMapping = {
  '1/2l': {
    row: 2,
    content: { order: 1 },
    image: { order: 2 },
  },
  '1/2r': {
    row: 2,
    content: { order: 2 },
    image: { order: 1 },
  },
  '1/3l': {
    row: 3,
    content: { order: 1 },
    image: { order: 2, span: 8 },
  },
  '1/3r': {
    row: 3,
    content: { order: 2 },
    image: { order: 1, span: 8 },
  },
}

export const ContentRow = ({ children, noGutters, middleAlign, layout }) => {
  return (
    <Row
      noGutters={noGutters}
      sm={layoutRowMapping[layout].row}
      className={clsx({ 'align-items-center': middleAlign })}
    >
      {children}
    </Row>
  )
}

export const ContentColumn = ({ className, layout, title, description }) => {
  const contentProps = layoutRowMapping[layout].content
  return (
    <Col
      sm={contentProps}
      className={clsx('position-relative zindex-1', className)}
    >
      <SectionHeader>{title}</SectionHeader>
      <HtmlContent>{description}</HtmlContent>
    </Col>
  )
}

export const ImageColumn = ({ image, title, className, video, layout }) => {
  const imageProps = layoutRowMapping[layout].image
  return (
    <Col className='align-self-stretch d-flex flex-grow' sm={imageProps}>
      {image && (
        <div className={clsx(className, 'align-self-stretch w-100')}>
          {video ? (
            <LiteralVideo src={image} title={title} />
          ) : (
            <LiteralImage src={image} title={title} />
          )}
        </div>
      )}
    </Col>
  )
}
