import { ResponsiveEmbed } from 'react-bootstrap'

export const LiteralImage = ({ src, title }) => {
  return (
    <img src={src} alt={title} className='w-100 h-100 object-fit-contain' />
  )
}

export const LiteralVideo = ({ src, title }) => {
  return (
    <ResponsiveEmbed aspectRatio='16by9'>
      <iframe src={src} title={title} />
    </ResponsiveEmbed>
  )
}
