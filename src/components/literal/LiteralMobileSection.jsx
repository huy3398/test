import clsx from 'clsx'
import Section, { SectionHeader } from '../section'
import HtmlContent from '../html/HtmlContent'
import { ContentColumn, ContentRow, ImageColumn } from './Content'

const LiteralMobileSection = ({
  title,
  description,
  image,
  className,
  imageClassName,
  layout,
  video,
  valign,
  contentClassName,
  noGutters,
}) => {
  return (
    <Section className={clsx(className, 'px-4 position-relative')}>
      {layout ? (
        <ContentRow
          layout={layout}
          middleAlign={valign === 'middle'}
          noGutters={noGutters}
        >
          <ContentColumn
            className={contentClassName}
            title={title}
            description={description}
            layout={layout}
          />
          <ImageColumn
            className={imageClassName}
            title={title}
            image={image}
            layout={layout}
            video={video}
          />
        </ContentRow>
      ) : (
        <>
          <SectionHeader>{title}</SectionHeader>
          <HtmlContent>{description}</HtmlContent>
        </>
      )}
    </Section>
  )
}

export default LiteralMobileSection
