import clsx from 'clsx'
import Section, { SectionHeader } from '../section'
import HtmlContent from '../html/HtmlContent'
import { ContentColumn, ContentRow, ImageColumn } from './Content'

const LiteralSection = ({
  title,
  description,
  image,
  className,
  imageClassName,
  containerClassName,
  layout,
  video,
  valign,
  contentClassName,
  noGutters,
}) => {
  return (
    <Section
      className={clsx(className, 'py-6 px-4 position-relative')}
      containerClassName={containerClassName}
    >
      {layout ? (
        <ContentRow
          layout={layout}
          middleAlign={valign === 'middle'}
          noGutters={noGutters}
        >
          <ContentColumn
            className={contentClassName}
            title={title}
            description={description}
            layout={layout}
          />
          <ImageColumn
            className={imageClassName}
            title={title}
            image={image}
            layout={layout}
            video={video}
          />
        </ContentRow>
      ) : (
        <>
          <SectionHeader>{title}</SectionHeader>
          <HtmlContent>{description}</HtmlContent>
        </>
      )}
    </Section>
  )
}

export default LiteralSection
