import { Jumbotron, Container, Row, Col } from 'react-bootstrap'
import clsx from 'clsx'
import styles from './banner.module.scss'
import { SectionHeader } from '../section'
import HtmlContent from '../html/HtmlContent'

const Banner = ({
  title,
  description,
  background,
  layout,
  className,
  contentClassName,
}) => {
  const hasContent = !!title || !!description
  return (
    <Jumbotron
      fluid
      className={clsx(
        'bg-cover position-relative mb-0 d-flex align-items-center',
        styles.banner,
        className,
      )}
      style={{ backgroundImage: `url(${background})` }}
    >
      {hasContent && (
        <Container fluid='xl'>
          <Row>
            <Col
              xl={{ span: 6, offset: layout === '1/2r' ? 6 : 0 }}
              className={clsx(styles.bannerContent, contentClassName)}
            >
              <SectionHeader>{title}</SectionHeader>
              <HtmlContent className='font-size-lg'>{description}</HtmlContent>
            </Col>
          </Row>
        </Container>
      )}
    </Jumbotron>
  )
}
export default Banner
