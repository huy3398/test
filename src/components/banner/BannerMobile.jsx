import { Jumbotron, Container, Row, Col } from 'react-bootstrap'
import clsx from 'clsx'
import styles from './banner.module.scss'
import { SectionHeader } from '../section'
import HtmlContent from '../html/HtmlContent'

const BannerMobile = ({ title, description, background, className }) => {
  const hasContent = !!title || !!description
  return (
    <>
      <Jumbotron
        fluid
        className={clsx(
          'bg-cover position-relative mb-0 d-flex align-items-center',
          styles.bannerMobile,
          className,
        )}
        style={{ backgroundImage: `url(${background})` }}
      />
      {hasContent && (
        <Container fluid='xl'>
          <Row>
            <Col>
              <SectionHeader>{title}</SectionHeader>
              <HtmlContent className='font-size-lg'>{description}</HtmlContent>
            </Col>
          </Row>
        </Container>
      )}
    </>
  )
}
export default BannerMobile
