import clsx from 'clsx'
import Link from 'next/link'
import { ListGroup } from 'react-bootstrap'
import useSWR from 'swr'
import { api } from '../../utils/url'
import Section from '../section'
import styles from './categories.module.scss'

const CategoriesSection = ({ title, categories: initialData }) => {
  const { data: categories = [] } = useSWR(api('article-categories',{},false), { initialData })
  return (
    <Section sidebar title={title}>
      {categories.length > 0 && (
        <ListGroup className='border overflow-hidden bg-white'>
          {categories.map(item => {
            return (
              <Link
                key={item.slug}
                href={`/blog/category/${item.slug}`}
                passHref
              >
                <ListGroup.Item
                  as='a'
                  className={clsx(
                    'd-flex align-items-center border-0 text-dark bg-transparent',
                    styles.item,
                  )}
                >
                  <span className='flex-grow-1'>{item.name}</span>{' '}
                  <strong>({item.total})</strong>
                </ListGroup.Item>
              </Link>
            )
          })}
        </ListGroup>
      )}
    </Section>
  )
}

export default CategoriesSection
