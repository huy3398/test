import Image from 'next/image'
import { Col, Media, Row } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'
import Section, { SectionHeader } from '../section'
import styles from './whywata.module.scss'

const WhyWataSection = ({ items, title }) => {
  return (
    <Section containerClassName={styles.whywata}>
      <SectionHeader align='center'>{title}</SectionHeader>

      <Row>
        <Col lg={8} className='d-flex'>
          <Row lg={2}>
            {items.map(item => {
              return (
                <Col key={`whywata-${item.id}`}>
                  <Media className='mb-5'>
                    <Image
                      src={item.icon}
                      alt={item.title}
                      width='70px'
                      height='70px'
                    />
                    <Media.Body className='ml-4'>
                      <h3 className='h5'>{item.title}</h3>
                      <HtmlContent>{item.description}</HtmlContent>
                    </Media.Body>
                  </Media>
                </Col>
              )
            })}
          </Row>
        </Col>
      </Row>
    </Section>
  )
}

export default WhyWataSection
