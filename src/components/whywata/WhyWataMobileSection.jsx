import Image from 'next/image'
import { Media } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'
import Section, { SectionHeader } from '../section'

const WhyWataMobileSection = ({ items, title }) => {
  return (
    <Section>
      <SectionHeader align='center'>{title}</SectionHeader>
      <div className='row'>
        {items.map(item => {
          return (
            <div className='col-12' key={item.id}>
              <Media className='mb-5'>
                <Image
                  src={item.icon}
                  alt={item.title}
                  width='70px'
                  height='70px'
                />
                <Media.Body className='ml-4'>
                  <h3 className='h5'>{item.title}</h3>
                  <HtmlContent>{item.description}</HtmlContent>
                </Media.Body>
              </Media>
            </div>
          )
        })}
      </div>
    </Section>
  )
}

export default WhyWataMobileSection
