/* eslint-disable react/no-array-index-key */
import clsx from 'clsx'
import { Row, Col } from 'react-bootstrap'
import Section, { SectionHeader } from '../section'
import { groupedBy } from '../../utils/array'
import styles from './technologies.module.scss'
import commonStyles from '../section/section.module.scss'

const TechnologiesSection = ({
  items,
  title,
  isDesktopOrLaptop,
  isTabletOrMobile,
}) => {
  return (
    <Section
      className={clsx('py-2 position-relative', {
        [commonStyles.doubleBackground]: isDesktopOrLaptop,
      })}
    >
      <SectionHeader align='center'>{title}</SectionHeader>
      <div className={clsx('mx-auto', styles.list)}>
        {isDesktopOrLaptop &&
          groupedBy(items, 5).map((group, index) => {
            return (
              <Row key={`tech-${index}`} className='d-flex my-4'>
                {group.map(elm => {
                  return (
                    <Col key={`item-${elm.id}`}>
                      <img
                        src={elm.icon}
                        width='120'
                        height='60'
                        className='d-block object-fit-contain my-3 mx-auto'
                        alt={elm.title}
                      />
                    </Col>
                  )
                })}
              </Row>
            )
          })}
        {isTabletOrMobile &&
          groupedBy(items, 3).map((group, index) => {
            return (
              <Row key={`tech-${index}`} className='d-flex my-4'>
                {group.map(elm => {
                  return (
                    <Col key={`item-${elm.id}`}>
                      <img
                        src={elm.icon}
                        width='60'
                        height='30'
                        className='d-block object-fit-contain my-3 mx-auto'
                        alt={elm.title}
                      />
                    </Col>
                  )
                })}
              </Row>
            )
          })}
      </div>
    </Section>
  )
}

export default TechnologiesSection
