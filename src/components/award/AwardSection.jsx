import { Col, Row } from 'react-bootstrap'
import Section, { SectionHeader } from '../section'
import { Carousel } from 'react-bootstrap'
import {getTotalItems} from '../../utils/carousel'


const Award = ({ title, items }) => {

const next = getTotalItems([...items], 8);
  return (
    <Section className='py-6'>
      <SectionHeader align='center'>{title}</SectionHeader>
      <Carousel>
        <Carousel.Item>
          <Row className='d-flex flex-row'>
            {items.slice(0, next.length).map(item => {
              return (
                <Col lg={3} key={`award-${item.id}`} className='mb-4'>
                  <a
                    className='d-block shadow px-4 py-3 rounded'
                    href={item.linkTo || '#'}
                    target='_blank'
                    rel='noreferrer'
                  >
                    <img
                      src={item.image}
                      width='100%'
                      height='170px'
                      className='object-fit-contain'
                      alt={item.title}
                    />
                  </a>
                </Col>
              )
            })}
          </Row>

        </Carousel.Item>
      </Carousel>
      
    </Section>
  )
}

export default Award
