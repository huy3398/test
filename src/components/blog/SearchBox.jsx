import clsx from 'clsx'
import { FormControl, InputGroup, Button, Form } from 'react-bootstrap'
import { useRouter } from 'next/router'
import { BsSearch } from 'react-icons/bs'
import Section from '../section'
import styles from './search.module.scss'

const SearchBox = ({ className }) => {
  const router = useRouter()
  const { q } = router.query
  const submitSearch = e => {
    e.preventDefault()
    const searchQuery = e.target?.elements?.q?.value
    const route = {
      pathname: '/blog',
    }
    if (searchQuery) route.query = { q: searchQuery }
    router.push(route, undefined, { scroll: false })
  }
  return (
    <Section sidebar className={className}>
      <Form onSubmit={submitSearch}>
        <InputGroup className='mb-3 rounded'>
          <InputGroup.Prepend>
            <Button
              variant='outline-gray-light'
              className={clsx(
                styles.button,
                'text-gray-light border-right-0 rounded-left',
              )}
            >
              <BsSearch size={20} />
            </Button>
          </InputGroup.Prepend>
          <FormControl
            placeholder='Search'
            className='border-left-0 rounded-right'
            name='q'
            defaultValue={q}
          />
        </InputGroup>
      </Form>
    </Section>
  )
}

export default SearchBox
