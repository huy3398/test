import useSWR from 'swr'
import Section from '../section'
import { api } from '../../utils/url'
import ContentList from '../content-list/ContentList'

const layoutMapping = {
  '1-col': 1,
  '2-col': 2,
  '3-col': 3,
}

const BlogSection = ({
  title,
  layout,
  articles: initialData,
  isTabletOrMobile,
  rowCol4,
}) => {
  const { data: articles } = useSWR(api('articles', { size: 3 }, false), {
    initialData,
  })
  return (
    <Section containerClassName='my-6' title={title}>
      <ContentList
        data={articles?.data || []}
        columns={layoutMapping[layout]}
        baseUrl='/blog'
        isTabletOrMobile={isTabletOrMobile}
        rowCol4={rowCol4}
      />
    </Section>
  )
}

export default BlogSection
