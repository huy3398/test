import { Button } from 'react-bootstrap'
import clsx from 'clsx'

const NavIndicators = ({ items, goToIndex, activeIndex, isTabletOrMobile }) => {
  return (
    <div className={isTabletOrMobile ? '' : 'mt-5'}>
      {!isTabletOrMobile && (
        <ol className='d-flex list-unstyled justify-content-center'>
          {items.map((item, index) => {
            return (
              <li key={`indicator-${item.id}`} className='m-y p-0 mx-4'>
                <Button
                  variant='white'
                  onClick={() => goToIndex(index)}
                  className='p-2 mx-2'
                >
                  <img
                    src={item.icon}
                    alt={item.meta}
                    className={clsx({
                      'filter-gray': activeIndex !== index,
                    })}
                  />
                </Button>
              </li>
            )
          })}
        </ol>
      )}
      {isTabletOrMobile && (
        <ol className='d-flex list-unstyled justify-content-center row'>
          {items.map((item, index) => {
            return (
              <li key={`indicator-${item.id}`} className='p-0 mx-4 col-12'>
                <Button variant='white' onClick={() => goToIndex(index)}>
                  <img
                    src={item.icon}
                    alt={item.meta}
                    width='50px'
                    className={clsx({
                      'filter-gray': activeIndex !== index,
                    })}
                  />
                </Button>
              </li>
            )
          })}
        </ol>
      )}
    </div>
  )
}

export default NavIndicators
