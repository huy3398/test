import clsx from 'clsx'
import { useState } from 'react'
import { Carousel } from 'react-bootstrap'
import Section from '../section'
import NavIndicators from './NavIndicators'
import TestimonialItem from './TestimonialItem'
import styles from './testimonials.module.scss'

const TestimonialsSection = ({ title, items, isTabletOrMobile }) => {
  const [active, setActive] = useState(0)

  return (
    <Section title={title} containerClassName='mt-2'>
      <div className='row'>
        <div className='col-2 col-md-12'>
          <NavIndicators
            items={items}
            goToIndex={setActive}
            activeIndex={active}
            isTabletOrMobile={isTabletOrMobile}
          />
        </div>
        <div className='col-10 col-md-12'>
          <Carousel
            slide
            className={clsx(styles.carousel)}
            controls={false}
            indicators={false}
            activeIndex={active}
          >
            {items.map(item => {
              return (
                <Carousel.Item
                  key={`carousel-item-${item.id}`}
                  className={clsx(
                    'border border-gray-lighter row',
                    styles.item,
                  )}
                >
                  <div className='col-12'>
                    <TestimonialItem
                      {...item}
                      isTabletOrMobile={isTabletOrMobile}
                    />
                  </div>
                </Carousel.Item>
              )
            })}
          </Carousel>
        </div>
      </div>
    </Section>
  )
}

export default TestimonialsSection
