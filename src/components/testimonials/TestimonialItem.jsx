import { Col, Row } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'

const TestimonialItem = ({
  title,
  description,
  image,
  meta,
  isTabletOrMobile,
}) => {
  return (
    <Row xl={2} className={isTabletOrMobile ? 'ml-5' : 'mx-1'}>
      <Col sm={isTabletOrMobile ? '{{ span: 12 }}' : null}>
        <article>
          <header>
            <h3 className='h4 font-weight-bold text-uppercase'>{title}</h3>
            <p className='lead text-primary'>{meta}</p>
          </header>
          <HtmlContent as='blockquote'>{description}</HtmlContent>
        </article>
      </Col>
      <Col sm={isTabletOrMobile ? '{{ span: 12 }}' : null}>
        <img src={image} alt={title} width='100%' />
      </Col>
    </Row>
  )
}

export default TestimonialItem
