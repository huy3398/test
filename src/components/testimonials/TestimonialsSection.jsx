import clsx from 'clsx'
import { useState } from 'react'
import { Carousel } from 'react-bootstrap'
import Section from '../section'
import NavIndicators from './NavIndicators'
import TestimonialItem from './TestimonialItem'
import styles from './testimonials.module.scss'

const TestimonialsSection = ({ title, items }) => {
  const [index, setIndex] = useState(0)

  const handleSelect = selectedIndex => {
    setIndex(selectedIndex)
  }

  return (
    <Section title={title} containerClassName='mt-6'>
      <Carousel
        className={clsx('mx-6', styles.carousel)}
        activeIndex={index}
        onSelect={handleSelect}
      >
        {items.map(item => {
          return (
            <Carousel.Item
              interval={5000}
              key={`carousel-item-${item.id}`}
              className={clsx('p-5 border border-gray-lighter', styles.item)}
            >
              <TestimonialItem {...item} />
            </Carousel.Item>
          )
        })}
      </Carousel>
      <NavIndicators
        items={items}
        goToIndex={handleSelect}
        activeIndex={index}
      />
    </Section>
  )
}

export default TestimonialsSection
