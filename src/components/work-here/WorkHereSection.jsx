import { Container } from 'react-bootstrap'
import clsx from 'clsx'
import SectionHeader from '../section/SectionHeader'
import HtmlContent from '../html/HtmlContent'
import sectionStyles from '../section/section.module.scss'
import styles from './workhere.module.scss'

const WorkHereSection = ({ title, description, image }) => {
  return (
    <section className={clsx(sectionStyles.section, 'position-relative pt-6')}>
      <SectionHeader align='center'>{title}</SectionHeader>
      <div
        className={clsx(
          sectionStyles.doubleBackground,
          'position-relative p-5',
        )}
      >
        <Container fluid='xl'>
          <div className='d-flex shadow overflow-hidden rounded bg-white'>
            <div className={clsx('p-5', styles.content)}>
              <HtmlContent>{description}</HtmlContent>
            </div>
            <div className='w-75'>
              <img
                src={image}
                alt={title}
                className='d-block object-fit-cover'
                width='100%'
                height='400px'
              />
            </div>
          </div>
        </Container>
      </div>
    </section>
  )
}

export default WorkHereSection
