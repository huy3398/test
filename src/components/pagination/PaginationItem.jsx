import { Pagination } from 'react-bootstrap'
import clsx from 'clsx'

const PaginationItem = ({ currentPage, page, onClick }) => {
  return (
    <Pagination.Item
      active={page === currentPage}
      className={clsx('mx-1', 'font-weight-bold', {
        'shadow-sm': page === currentPage,
      })}
      onClick={onClick}
    >
      {page}
    </Pagination.Item>
  )
}

export default PaginationItem
