import { Pagination as BsPagination } from 'react-bootstrap'
import { BsChevronLeft, BsChevronRight } from 'react-icons/bs'
import { useRouter } from 'next/router'
import clsx from 'clsx'
import qs from 'qs'
import PaginationItem from './PaginationItem'
import styles from './pagination.module.scss'
import { arrayFrom } from '../../utils/array'

const Pagination = ({ pageCount, currentPage }) => {
  const router = useRouter()
  const goToPage = page => {
    router.push(`${router.pathname}?${qs.stringify({ page })}`)
  }
  const showNumber = p => {
    return (
      p < 3 ||
      p > pageCount - 2 ||
      p === currentPage ||
      p === currentPage - 1 ||
      p === currentPage + 1 ||
      (p === 3 && currentPage === 5) ||
      (p === pageCount - 2 && currentPage === pageCount - 4)
    )
  }
  const showEllipsis = p => {
    return (
      (p === 3 && currentPage > 5) ||
      (p === pageCount - 2 && currentPage < pageCount - 4)
    )
  }

  return (
    <>
      <BsPagination className='justify-content-center'>
        <BsPagination.Prev
          disabled={currentPage - 1 <= 0}
          className={clsx('mx-1 shadow-sm', styles.itemWhite)}
          onClick={() => goToPage(currentPage - 1)}
        >
          <BsChevronLeft />
        </BsPagination.Prev>
        {arrayFrom(pageCount).map(elm => {
          if (showNumber(elm)) {
            return (
              <PaginationItem
                onClick={() => goToPage(elm)}
                key={`${router.pathname}?${qs.stringify({ elm })}`}
                page={elm}
                currentPage={currentPage}
              />
            )
          }
          if (showEllipsis(elm)) {
            return (
              <BsPagination.Ellipsis
                key={`${router.pathname}?${qs.stringify({ elm })}`}
                className={clsx(styles.itemWhite, 'font-weight-bold')}
              />
            )
          }

          return null
        })}
        <BsPagination.Next
          disabled={currentPage >= pageCount}
          className={clsx('mx-1 shadow-sm', styles.itemWhite)}
          onClick={() => goToPage(currentPage + 1)}
        >
          <BsChevronRight />
        </BsPagination.Next>
      </BsPagination>
    </>
  )
}

export default Pagination
