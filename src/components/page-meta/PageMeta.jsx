import Head from 'next/head'
import getConfig from 'next/config'
import { strip } from '../../utils/string'
import {
  blog,
  breadcrumb,
  career,
  getProps,
  ogType,
  org,
  pageTypes,
  typeUrl,
} from './jsonLd'

const { publicRuntimeConfig: config } = getConfig()

const PageMeta = ({ data: initData, page = 1, type = pageTypes.PAGE }) => {
  const defaultData = {
    title: config.company.name,
    description: config.company.description,
    image: config.company.logo,
  }

  const data = { ...defaultData, ...initData }

  const metaDescription = strip(data.description, 160)
  return (
    <Head>
      <title>{data.title}</title>
      <meta httpEquiv='Content-Type' content='text/html; charset=utf-8' />
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <meta name='description' content={metaDescription} />

      <meta property='og:url' content={typeUrl(data.slug, type, page)} />
      <meta property='og:type' content={ogType(type)} />
      <meta property='og:locale' content='vi-VN' />
      <meta property='og:title' content={data.title} />
      {metaDescription && (
        <meta property='og:description' content={metaDescription} />
      )}
      {data.image && (
        <meta property='og:image' content={data.image || '/img/Logo.svg'} />
      )}

      <link rel='icon' type='image/svg+xml' href='/img/logo-sm.svg' />
      <link rel='mask-icon' href='/img/logo-sm.svg' />

      <script {...getProps(org())} />

      {(type === pageTypes.BLOG || type === pageTypes.WORK) && (
        <script {...getProps(blog(data, type))} />
      )}

      {type === pageTypes.CAREER && <script {...getProps(career(data))} />}

      <script {...getProps(breadcrumb(data, type))} />
    </Head>
  )
}

export default PageMeta
