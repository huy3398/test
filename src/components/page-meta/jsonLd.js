import getConfig from 'next/config'
import { absolute } from '../../utils/url'

const { publicRuntimeConfig: config } = getConfig()

export const pageTypes = {
  PAGE: 'page',
  BLOG: 'article',
  WORK: 'work',
  CAREER: 'job',
  CATEGORY: 'category',
}

export const ogType = type => {
  const mapping = {
    [pageTypes.PAGE]: 'website',
    [pageTypes.CATEGORY]: 'website',
    [pageTypes.BLOG]: 'article',
    [pageTypes.WORK]: 'article',
    [pageTypes.CAREER]: 'article',
  }
  return mapping[type]
}

export const typeUrl = (slug = '', type = pageTypes.PAGE, page = 1) => {
  const params = page === 1 ? {} : { page }
  if (type === pageTypes.PAGE) {
    return absolute(`/${slug}`, params)
  }
  if (type === pageTypes.BLOG) {
    return absolute(`/blog/${slug}`, params)
  }
  if (type === pageTypes.CAREER) {
    return absolute(`/careers/${slug}`, params)
  }
  if (type === pageTypes.WORK) {
    return absolute(`/works/${slug}`, params)
  }
  if (type === pageTypes.CATEGORY) {
    return absolute(`/blog/category/${slug}`, params)
  }

  return absolute()
}

export const getProps = data => {
  return {
    type: 'application/ld+json',
    dangerouslySetInnerHTML: { __html: JSON.stringify(data) },
  }
}

const address = {
  '@type': 'PostalAddress',
  streetAddress: config.company.address.streetAddress,
  addressRegion: config.company.address.region,
  addressLocality: config.company.address.locality,
  postalCode: config.company.address.postalCode,
  addressCountry: 'VN',
}

const company = {
  '@type': 'Organization',
  name: config.company.name,
  description: config.company.description,
  logo: absolute('/img/Logo.svg'),
  url: typeUrl(),
}

export const org = () => ({
  '@context': 'https://schema.org',
  '@id': typeUrl(),
  ...company,
  telephone: config.contact.phone,
  email: config.contact.email,
  address,
})

export const career = job => ({
  '@context': 'https://schema.org/',
  '@type': 'JobPosting',
  title: job.title,
  description: job.description,
  datePosted: job.datePosted,
  validThrough: job.validThrough,
  employmentType: job.employmentType,
  hiringOrganization: company,
  jobLocation: {
    '@type': 'Place',
    address,
  },
  baseSalary: {
    '@type': 'MonetaryAmount',
    currency: 'VND',
    value: job.baseSalary,
  },
})

export const blog = (article, type = pageTypes.BLOG) => ({
  '@context': 'https://schema.org',
  '@type': 'Article',
  headline: article.title,
  datePublished: article.datePublished,
  image: article.images,
  publisher: company,
  mainEntityOfPage: {
    '@type': 'WebPage',
    '@id': typeUrl(type, article.slug),
  },
})

export const breadcrumb = (item, type = pageTypes.PAGE) => {
  const itemListElement = [
    {
      '@type': 'ListItem',
      position: 1,
      name: config.company.name,
      item: typeUrl(),
    },
  ]

  if (item) {
    if (type === pageTypes.PAGE && item.slug && item.slug !== 'index') {
      itemListElement.push({
        '@type': 'ListItem',
        position: 2,
        name: item.title,
        item: typeUrl(item.slug),
      })
    }

    if (type === pageTypes.BLOG || type === pageTypes.CATEGORY) {
      itemListElement.push({
        '@type': 'ListItem',
        position: 2,
        name: 'Blog',
        item: typeUrl('blog'),
      })

      itemListElement.push({
        '@type': 'ListItem',
        position: 3,
        name: item.title,
        item: typeUrl(item.slug, type),
      })
    }

    if (type === pageTypes.WORK) {
      itemListElement.push({
        '@type': 'ListItem',
        position: 2,
        name: 'Our Works',
        item: typeUrl('works'),
      })

      itemListElement.push({
        '@type': 'ListItem',
        position: 3,
        name: item.title,
        item: typeUrl(item.slug, pageTypes.WORK),
      })
    }

    if (type === pageTypes.CAREER) {
      itemListElement.push({
        '@type': 'ListItem',
        position: 2,
        name: 'Careers',
        item: typeUrl('careers'),
      })

      itemListElement.push({
        '@type': 'ListItem',
        position: 3,
        name: item.title,
        item: typeUrl(item.slug, pageTypes.CAREER),
      })
    }
  }

  return {
    '@context': 'https://schema.org',
    '@type': 'BreadcrumbList',
    itemListElement,
  }
}
