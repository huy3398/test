import { useState } from 'react'
import { Button } from 'react-bootstrap'
import ApplyModal from './ApplyModal'

const ApplyButton = ({ job }) => {
  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  return (
    <>
      <Button
        size='lg'
        className='shadow py-3 text-uppercase w-50'
        onClick={handleShow}
      >
        Apply now
      </Button>

      <ApplyModal job={job} show={show} onHide={handleClose} />
    </>
  )
}

export default ApplyButton
