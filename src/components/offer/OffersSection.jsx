import clsx from 'clsx'
import { ListGroup } from 'react-bootstrap'
import useSWR from 'swr'
import { api } from '../../utils/url'
import Section, { SectionHeader } from '../section'
import OfferItem from './OfferItem'
import styles from './offers.module.scss'

const OffersSection = ({ title, description, jobs: initialData }) => {

  const { data: jobs = [] } = useSWR(api('jobs', {}, false), {
    initialData,
  })

  return (
    <Section className='py-6'>
      <SectionHeader meta={description} align='center'>
        {title}
      </SectionHeader>
      <ListGroup className={clsx('my-5 rounded-0 mx-auto', styles.list)}>
        {jobs.map(job => (
          <OfferItem key={`job-${job.id}`} {...job} />
        ))}
      </ListGroup>
    </Section>
  )
}

export default OffersSection
