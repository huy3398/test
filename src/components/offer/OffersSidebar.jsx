import clsx from 'clsx'
import Link from 'next/link'
import { ListGroup } from 'react-bootstrap'
import useSWR from 'swr'
import { relative } from '../../utils/datetime'
import { api } from '../../utils/url'
import styles from './offers.module.scss'

const OffersSidebar = ({ title, jobs: initialData }) => {
  const { data: jobs = [] } = useSWR(api(`jobs`, {}, false), { initialData })
  return (
    <section>
      <header className='mb-2'>
        <h3>{title}</h3>
      </header>
      <ListGroup className={clsx(styles.sidebarList, 'border-0 rounded-0')}>
        {jobs.map(job => (
          <Link key={`job-${job.id}`} href={`/careers/${job.slug}`} passHref>
            <ListGroup.Item
              as='a'
              className={clsx(
                styles.sidebarItem,
                'px-0 py-4 border-left-0 border-right-0 border-top-0',
              )}
            >
              <h4>{job.title}</h4>
              <div className='text-muted font-size-sm'>
                <time dateTime={job.postedDate} className='mr-1'>
                  Posted {relative(new Date(job.postedDate))}
                </time>
                {/* &bull;
                <span className='ml-1'>{readingTime(job.content)}</span> */}
              </div>
            </ListGroup.Item>
          </Link>
        ))}
      </ListGroup>
    </section>
  )
}

export default OffersSidebar
