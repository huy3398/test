import clsx from 'clsx'
import Link from 'next/link'
import { Badge, ListGroup } from 'react-bootstrap'
import { BsChevronRight } from 'react-icons/bs'
import styles from './offers.module.scss'

const OfferItem = ({ slug, title, status }) => {
  return (
    <Link href={`/careers/${slug}`} passHref>
      <ListGroup.Item
        as='a'
        className={clsx(
          styles.item,
          'font-family-display border-left-0 border-right-0 d-flex align-items-center text-dark',
        )}
      >
        <div className='flex-grow-1'>{title}</div>
        {status === 'NEW' && (
          <Badge variant='primary text-uppercase rounded-sm'>New</Badge>
        )}
        <BsChevronRight className='ml-3' />
      </ListGroup.Item>
    </Link>
  )
}

export default OfferItem
