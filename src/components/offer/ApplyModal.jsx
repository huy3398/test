import { yupResolver } from '@hookform/resolvers/yup'
import clsx from 'clsx'
import { Button, Form, Modal } from 'react-bootstrap'
import { useForm } from 'react-hook-form'
import { BsReplyAll, BsUpload, BsX } from 'react-icons/bs'
import { useFormUtils } from '../../hooks/useFormUtils'
import schema from './schema'
import styles from './offers.module.scss'
import { apiV2 } from '../../utils/url'

const ApplyModal = ({ show, onHide, job }) => {
  const { register, handleSubmit, formState, formState: { errors }, setValue, reset } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const { isInvalid, errorMsg, isDisabled } = useFormUtils(formState)

  let resumeErrMsg = ''
  const objectIsNull = errors.resume ?? 'File Upload is errors'
  if (objectIsNull !== 'File Upload is errors') {
    const resumeErrors = Object.keys(errors?.resume)
    resumeErrMsg = resumeErrors.length > 0 ? errors.resume[resumeErrors[0]].message
      : errors.resume[resumeErrors[1]].message
    if (resumeErrMsg.includes('must be less') || resumeErrMsg.includes('File is not supported')) {
      resumeErrMsg = 'We accept .doc .docx, .pdf, images files up to 10MB'
    }
    else {
      resumeErrMsg = 'Please choose file CV'
    }
  }


const submit = async (data) => {
  const formData = new FormData();
  formData.append('file', data.resumeFile[0])
  let obj = { email: data.email, name: data.name, phoneNumber: data.phone }
  const res = await fetch(apiV2('upload-cv'), {
    method: 'POST',
    body: formData
  })
    .then(res => res.json())
    .then(async data => {
      if (data?.status === 'SUCCESS') {
        obj.attachedDocuments = data.link
        const rest = await fetch(apiV2('applications'), {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(obj)
        }).then(rest => rest.json())
          .then(data1 => {
            console.log("success", data1)
            reset()
          })
      }
    })
}

return (
  <Modal
    show={show}
    onHide={onHide}
    size='lg'
    contentClassName={styles.modal}
  >
    <Modal.Body>
      <Button
        variant='white'
        onClick={onHide}
        className={clsx(
          'position-absolute top-0 right-0 p-1 rounded-circle',
          styles.modalClose,
        )}
      >
        <BsX size={32} />
      </Button>
      <header className='text-center mb-4'>
        <h5 className='text-lg font-weight-regular'>
          You&lsquo;re applying for
        </h5>
        <h3 className='h4 font-weight-bold'>{job.title}</h3>
      </header>
      <Form onSubmit={handleSubmit(submit)}>
        <Form.Group controlId='apply-name'>
          <Form.Control
            placeholder='Full name'
            isInvalid={isInvalid('name')}
            {...register('name')}
          />
          <Form.Control.Feedback type='invalid'>
            {errorMsg('name')}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId='apply-phone'>
          <Form.Control
            placeholder='Phone number'
            isInvalid={isInvalid('phone')}
            {...register('phone')}
          />
          <Form.Control.Feedback type='invalid'>
            {errorMsg('phone')}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId='apply-email'>
          <Form.Control
            placeholder='Email'
            isInvalid={isInvalid('email')}
            {...register('email')}
          />
          <Form.Control.Feedback type='invalid'>
            {errorMsg('email')}
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group controlId='apply-resume'>
          <div className='d-flex align-items-center'>
            <Button variant='outline-dark' as='label' className='mb-0 mr-4'>
              <Form.Control
                type='file'
                onChange={(e) => {
                  const { type = '', name, size } = e.target.files[0]
                  setValue('resumeFile', e.target.files)
                  setValue('resume', { type, name, size }, { shouldValidate: true })
                }}
              />
            </Button>
            Maximum 10Mb
          </div>
          {resumeErrMsg && <p className='text-danger'>{resumeErrMsg}</p>}
        </Form.Group>
        <div className='text-center'>
          <Button
            variant='primary'
            onClick={onHide}
            size='lg'
            className='w-50 py-2 text-uppercase'
            type='submit'
            disabled={!formState.isDirty || !formState.isValid} 
          >
            Submit now
          </Button>

        </div>
      </Form>
    </Modal.Body>
  </Modal>
)
}

export default ApplyModal
