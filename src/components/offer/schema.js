import * as yup from 'yup'

const SUPPORTED_FORMATS = [
  'image/jpg',
  'image/jpeg',
  'image/gif',
  'image/png',
  'application/pdf',
  'application/msword',
  'text/plain',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
];

const schema = yup.object().shape({
  name: yup.string().required(),
  phone: yup.number().positive().typeError('you must specify a number'),
  email: yup.string().email().required(),
  resume: yup.object().shape({
		name: yup.string().required(),
		size: yup.number().max(1024 *1024*10).required(), // 100mb
		type: yup.string().test('fileType', 'File is not supported', function(value) {
			return SUPPORTED_FORMATS.includes(value)
		}),
	})
})

export default schema
