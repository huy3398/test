import clsx from 'clsx'
import Link from 'next/link'
import { forwardRef } from 'react'
import getConfig from 'next/config'

const { publicRuntimeConfig: config } = getConfig()

const Logo = forwardRef(({ width, as = 'a', className, ...props }, ref) => {
  const Component = as
  return (
    <Link href='/' passHref>
      <Component
        ref={ref}
        className={clsx('d-inline-block', className)}
        {...props}
      >
        <img
          width={width}
          src={config.company.logo}
          alt={config.company.name}
        />
      </Component>
    </Link>
  )
})

export default Logo
