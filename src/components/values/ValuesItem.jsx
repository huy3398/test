import { Card } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'
import styles from './values.module.scss'

const ValuesItem = ({ color, title, icon, description }) => {
  return (
    <Card bg={color} className='p-4 rounded-0'>
      <Card.Header className='px-2 pt-2 pb-2 bg-transparent d-flex'>
        <h3 className='text-white text-uppercase mb-0 h6 font-weight-semi-bold align-self-end flex-grow-1'>
          {title}
        </h3>
        <img src={icon} alt={title} className='flex-shrink-0 mb-4' />
      </Card.Header>
      <Card.Body className='text-white px-2 py-2'>
        <HtmlContent className={styles.valuesContent}>
          {description}
        </HtmlContent>
      </Card.Body>
    </Card>
  )
}
export default ValuesItem
