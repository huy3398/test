import { CardGroup } from 'react-bootstrap'
import Section from '../section'
import ValuesItem from './ValuesItem'

const ValuesSection = ({ items, isTabletOrMobile }) => {
  const bgColors = ['red', 'red-dark', 'red-darker']
  return (
    <Section>
      {!isTabletOrMobile && (
        <CardGroup>
          {items.map((item, index) => {
            const color = bgColors[index % 3]
            return (
              <ValuesItem key={`values-${item.id}`} {...item} color={color} />
            )
          })}
        </CardGroup>
      )}
      {isTabletOrMobile && (
        <div>
          {items.map((item, index) => {
            const color = bgColors[index % 3]
            return (
              <ValuesItem key={`values-${item.id}`} {...item} color={color} />
            )
          })}
        </div>
      )}
    </Section>
  )
}

export default ValuesSection
