import clsx from 'clsx'
import getConfig from 'next/config'

const { publicRuntimeConfig: config } = getConfig()

const GoogleMap = ({ className }) => {
  return (
    <iframe
      title={config.company.address.streetAddress}
      src={config.company.address.embedUrl}
      allowFullScreen
      width='100%'
      height='300px'
      frameBorder='0'
      loading='lazy'
      className={clsx('d-block border-none', className)}
    />
  )
}

export default GoogleMap
