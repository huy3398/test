import clsx from 'clsx'
import styles from './section.module.scss'

const SectionHeader = ({ children, meta, align, className, sidebar }) => {
  const headerStyle = sidebar
    ? 'h3 text-left text-uppercase mb-4'
    : 'text-uppercase mb-5 pb-4 position-relative font-weight-extra-bolder'
  return (
    <header
      className={clsx(styles.sectionHeader, className, {
        'text-center': align === 'center',
      })}
    >
      <h2
        className={clsx(headerStyle, {
          [styles.titleCenter]: !sidebar && align === 'center',
          [styles.title]: !sidebar && align !== 'center',
        })}
      >
        {children}
      </h2>
      {meta && (
        <div className='lead font-family-display font-weight-semi-bold'>
          {meta}
        </div>
      )}
    </header>
  )
}

export default SectionHeader
