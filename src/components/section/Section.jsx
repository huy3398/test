import { Container } from 'react-bootstrap'
import clsx from 'clsx'
import SectionHeader from './SectionHeader'
import styles from './section.module.scss'

const Section = ({
  title,
  meta,
  children,
  containerClassName,
  className,
  fluid,
  background,
  sidebar,
  isTabletOrMobile,
  ...props
}) => {
  return (
    <section
      {...props}
      className={clsx(className, styles.section, 'position-relative')}
      style={background ? { backgroundImage: `url(${background})` } : undefined}
    >
      {!isTabletOrMobile && (
        <Container fluid='xl' className={containerClassName}>
          {title && (
            <SectionHeader meta={meta} align='center' sidebar={sidebar}>
              {title}
            </SectionHeader>
          )}
          {children && <div>{children}</div>}
        </Container>
      )}
      {isTabletOrMobile && (
        <div fluid='xl' className={containerClassName}>
          {title && (
            <SectionHeader meta={meta} align='center' sidebar={sidebar}>
              {title}
            </SectionHeader>
          )}
          {children && <div>{children}</div>}
        </div>
      )}
    </section>
  )
}

export default Section
