import clsx from 'clsx'
import Link from 'next/link'
import { Card } from 'react-bootstrap'
import { relative } from '../../utils/datetime'
import HtmlContent from '../html/HtmlContent'

const ContentItem = ({
  slug,
  image,
  title,
  author,
  excerpt,
  // content,
  showExcerpt,
  horizontal,
  publishedDate,
  baseUrl,
  columns,
  projectItem,
  createdDate,
  endDate,
}) => {
  return (
    <Link href={`${baseUrl}/${slug}`} passHref>
      <Card as='a' className={clsx({ 'flex-row': horizontal })}>
        {columns === 1 && (
          <Card.Img
            variant='top'
            src={image}
            className={clsx({
              'w-30 align-self-center mr-2 rounded-0': horizontal,
            })}
          />
        )}
        {columns === 2  && (
          <Card.Img
            variant='top'
            src={image}
            className={clsx({
              'w-30 align-self-center mr-2 rounded-0': horizontal,
            })}
            style={{ height: '12rem' }}
          />
        )}

        {columns === 3  && (
          <Card.Img
            variant='top'
            src={image}
            className={clsx({
              'w-30 align-self-center mr-2 rounded-0': horizontal,
            })}
            style={{ height: '12rem' }}
          />
        )}

        <Card.Body className={clsx('px-0', { 'pt-0': horizontal })}>
          <Card.Title>{title}</Card.Title>
          <Card.Text as='div'>
            <p className='text-muted font-size-sm'>
              {!projectItem && (
                <>
                  <span>By {author.name}</span> &bull;
                  <time dateTime={publishedDate}>
                    {relative(new Date(publishedDate))}
                  </time>
                </>
              )}
              {projectItem && (
                <>
                  Project Duration: {relative(new Date(createdDate))} -{' '}
                  {relative(new Date(endDate))}
                </>
              )}
            </p>
            {showExcerpt && (
              <HtmlContent className='text-dark'>{excerpt}</HtmlContent>
            )}
          </Card.Text>
        </Card.Body>
      </Card>
    </Link>
  )
}

export default ContentItem
