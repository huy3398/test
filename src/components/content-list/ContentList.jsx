import clsx from 'clsx'
import ContentItem from './ContentItem'
import { Row, Col } from 'react-bootstrap'
const ContentList = ({
  data,
  columns,
  excerpt,
  horizontal,
  baseUrl,
  isTabletOrMobile,
  projectItem,
  rowCol4,
  rowCol6,
}) => {
  return (
    <Row lg={columns} className='row'>
      {data.map((item, index) => {
        return (
          <Col
            key={item.slug}
            className={clsx({ 'col-4': rowCol4, 'col-6': rowCol6 })}
          >
            {index !== 0 && isTabletOrMobile && <hr />}
            <ContentItem
              {...item}
              baseUrl={baseUrl}
              showExcerpt={excerpt}
              horizontal={horizontal}
              columns={columns}
              projectItem={projectItem}
            />
          </Col>
        )
      })}
    </Row>
  )
}

export default ContentList
