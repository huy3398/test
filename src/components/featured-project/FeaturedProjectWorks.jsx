import Link from 'next/link'
import { Card } from 'react-bootstrap'
import { relative } from '../../utils/datetime'

const FeaturedProjectWorks = ({
  slug,
  image,
  title,
  reviewdate,
  description,
}) => {
  return (
    <Link href={`/works/${slug}`} passHref>
      <Card as='a'>
        <Card.Img variant='top' src={image} />
        <Card.Body className='px-0'>
          <Card.Title>{title}</Card.Title>
          {description && (
            <Card.Text className='text-dark'>{description}</Card.Text>
          )}
          {reviewdate && (
            <Card.Text>
              <time className='text-muted' dateTime={reviewdate}>
                {relative(new Date(reviewdate))}
              </time>
            </Card.Text>
          )}
        </Card.Body>
      </Card>
    </Link>
  )
}

export default FeaturedProjectWorks
