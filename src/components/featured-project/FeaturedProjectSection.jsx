import clsx from 'clsx'
import Section from '../section'
import FeaturedProjectWorks from './FeaturedProjectWorks'
import Pagination from '../pagination/Pagination'
import { getTotalPages } from '../../utils/number'

const FeaturedProjectSection = ({
  title,
  page,
  works,
  pageSize,
  isDesktopOrLaptop,
}) => {
  const pageCount = getTotalPages(works?.total, pageSize)

  return (
    <Section
      containerClassName={isDesktopOrLaptop ? 'my-6' : 'my-4'}
      title={title}
    >
      <div className='row'>
        {works?.items?.map((project, index) => (
          <div
            key={index.toString()}
            className={clsx({
              'col-6 mb-2': !isDesktopOrLaptop,
              'col-4 mb-4': isDesktopOrLaptop,
            })}
          >
            <FeaturedProjectWorks {...project} />
          </div>
        ))}
      </div>

      {pageCount > 1 && (
        <Pagination pageCount={pageCount} currentPage={parseInt(page, 10)} />
      )}
    </Section>
  )
}

export default FeaturedProjectSection
