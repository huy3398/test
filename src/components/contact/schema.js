import * as yup from 'yup'

const schema = yup.object().shape({
  senderName: yup.string().required(),
  phoneNumber: yup.string().required(),
  senderEmail: yup.string().email().required(),
  subject: yup.string().required(),
  content: yup.string().required(),
})

export default schema
