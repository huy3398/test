import { Alert, Button, Col, Form, Row } from 'react-bootstrap'
import clsx from 'clsx'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { BsCheck } from 'react-icons/bs'
import getConfig from 'next/config'
import { ToastContainer, toast } from 'react-toastify'
import { useMediaQuery } from 'react-responsive'
import HtmlContent from '../html/HtmlContent'
import Section from '../section'
import SkypeButton from './SkypeButton'
import schema from './schema'
import { useFormUtils } from '../../hooks/useFormUtils'
import styles from './contact.module.scss'
import WhatsAppButton from './WhatsAppButton'
import 'react-toastify/dist/ReactToastify.css'
import { apiV2 } from '../../utils/url'

const { publicRuntimeConfig: config } = getConfig()

const ContactSection = ({ title, description }) => {
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  const { register, handleSubmit, formState } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  })

  const { isInvalid, errorMsg, isDisabled } = useFormUtils(formState)

  const submit = async data => {
    const response = await fetch(apiV2('send-message'), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })

    if (response?.status === 201) {
      toast.success('Successful')
    } else {
      toast.error('Fail')
    }

    return true
  }

  return (
    <Section
      className={clsx('py-5 position-relative', {
        [styles.bg]: !isTabletOrMobile,
      })}
      title={title}
    >
      <ToastContainer />
      <div className={clsx(styles.formDescription, 'mx-auto text-center')}>
        <HtmlContent className='mb-4'>{description}</HtmlContent>
        <p className='mb-4'>
          Or chat with us directly through{' '}
          <SkypeButton href={config.contact.skype}>Skype</SkypeButton>
          <WhatsAppButton number={config.contact.whatsapp}>
            WhatsApp
          </WhatsAppButton>
        </p>
      </div>
      <Form
        noValidate
        onSubmit={handleSubmit(submit)}
        className={clsx(styles.form, 'mx-auto')}
      >
        <Row>
          <Form.Group as={Col} controlId='contact-name' sm={{ span: 6 }}>
            <Form.Control
              disabled={isDisabled}
              placeholder='Your name'
              isInvalid={isInvalid('senderName')}
              {...register('senderName')}
            />
            <Form.Control.Feedback type='invalid'>
              {errorMsg('senderName')}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} controlId='contact-phone' sm={{ span: 6 }}>
            <Form.Control
              disabled={isDisabled}
              placeholder='Your phone'
              isInvalid={isInvalid('phoneNumber')}
              {...register('phoneNumber')}
            />
            <Form.Control.Feedback type='invalid'>
              {errorMsg('phoneNumber')}
            </Form.Control.Feedback>
          </Form.Group>
        </Row>
        <Row>
          <Form.Group as={Col} controlId='contact-email' sm={{ span: 6 }}>
            <Form.Control
              disabled={isDisabled}
              placeholder='Your email'
              isInvalid={isInvalid('senderEmail')}
              sm={{ span: 6 }}
              {...register('senderEmail')}
            />
            <Form.Control.Feedback type='invalid'>
              {errorMsg('senderEmail')}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} controlId='contact-subject' sm={{ span: 6 }}>
            <Form.Control
              disabled={isDisabled}
              placeholder='Subject'
              isInvalid={isInvalid('subject')}
              {...register('subject')}
            />
            <Form.Control.Feedback type='invalid'>
              {errorMsg('subject')}
            </Form.Control.Feedback>
          </Form.Group>
        </Row>
        <Form.Group controlId='contact-message'>
          <Form.Control
            disabled={isDisabled}
            as='textarea'
            placeholder='Message'
            isInvalid={isInvalid('content')}
            {...register('content')}
          />
          <Form.Control.Feedback type='invalid'>
            {errorMsg('content')}
          </Form.Control.Feedback>
        </Form.Group>
        {formState.isSubmitSuccessful && (
          <Alert variant='success'>
            Your message has been sent! Thank you for contacting us!
          </Alert>
        )}
        <div className='text-center'>
          <Button
            disabled={isDisabled}
            type='submit'
            size='lg'
            className={clsx('shadow py-2', styles.submit)}
            variant={formState.isSubmitSuccessful ? 'green' : 'primary'}
          >
            {formState.isSubmitSuccessful ? (
              <>
                <BsCheck /> Your message sent
              </>
            ) : (
              'Submit'
            )}
          </Button>
        </div>
      </Form>
    </Section>
  )
}

export default ContactSection
