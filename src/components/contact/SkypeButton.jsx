import clsx from 'clsx'
import { Button } from 'react-bootstrap'
import { FaSkype } from 'react-icons/fa'
import styles from './contact.module.scss'

const SkypeButton = ({ href, children }) => {
  return (
    <Button
      variant='skype'
      as='a'
      href={href}
      size='sm'
      target='_blank'
      className={clsx(styles.button, 'shadow-sm')}
    >
      <FaSkype fontSize='16px' /> {children}
    </Button>
  )
}

export default SkypeButton
