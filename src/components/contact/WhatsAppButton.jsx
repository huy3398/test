import { Button } from 'react-bootstrap'
import clsx from 'clsx'
import { FaWhatsapp } from 'react-icons/fa'
import { whatsapp } from '../../utils/url'
import styles from './contact.module.scss'

const WhatsAppButton = ({ number, children }) => {
  return (
    <Button
      variant='whatsapp'
      as='a'
      href={whatsapp(number)}
      size='sm'
      target='_blank'
      className={clsx(styles.button, 'shadow-sm')}
    >
      <FaWhatsapp fontSize='16px' /> {children}
    </Button>
  )
}

export default WhatsAppButton
