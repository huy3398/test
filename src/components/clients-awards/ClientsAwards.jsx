import clsx from 'clsx'
import { Row, Col } from 'react-bootstrap'
import { groupedBy } from '../../utils/array'
import Section, { SectionHeader } from '../section'
import styles from './clientawards.module.scss'
import { getTotalItems } from '../../utils/carousel'
import { Carousel } from 'react-bootstrap'

const ClientsAwardsSection = ({ items, title, background }) => {
  const groupedItems = groupedBy(items, 5)
  const next = getTotalItems([...items], 15);
  return (
    <Section
      className={clsx('py-6', styles.clientAward)}
      background={background}
    >
      <SectionHeader align='center'>{title}</SectionHeader>
      <Carousel>
        <Carousel.Item>
          <div className={clsx(styles.list, 'mx-auto')}>
            {groupedItems.slice(0, next.length).map((group, index) => {
              return (
                <Row lg={5} key={`client-award-${index}`} className='d-flex my-4'>
                  {group.map(elm => {
                    return (
                      <Col key={`item-${elm.id}`}>
                        <a
                          className={clsx(
                            'd-block shadow p-3 rounded',
                            styles.link,
                          )}
                          href={elm.linkTo || '#'}
                          target='_blank'
                          rel='noreferrer'
                        >
                          <img
                            src={elm.image}
                            width='100%'
                            height='40px'
                            className='object-fit-contain d-block'
                            alt={elm.title}
                          />
                        </a>
                      </Col>
                    )
                  })}
                </Row>
              )
            })}
          </div>

        </Carousel.Item>
      </Carousel>
    </Section>
  )
}

export default ClientsAwardsSection
