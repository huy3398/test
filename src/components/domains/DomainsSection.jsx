/* eslint-disable react/jsx-key */
import { Image } from 'react-bootstrap'
import Section, { SectionHeader } from '../section'

const DomainsSection = ({
  items,
  title,
  isDesktopOrLaptop,
  isTabletOrMobile,
}) => {
  return (
    <Section className='py-4'>
      <SectionHeader align='center'>{title}</SectionHeader>
      {isDesktopOrLaptop && (
        <div className='row pt-0 justify-content-center'>
          {items.map(item => {
            return (
              <div key={`domain-${item.id}`} className='col-2 py-4'>
                <div
                  className='shadow rounded-circle px-4 py-4 mx-auto'
                  style={{ width: 'fit-content' }}
                >
                  <Image src={item.icon} width='60' height='60' />
                </div>
                <p className='text-uppercase font-weight-bold mx-1 mt-4 mb-3 text-center'>
                  {item.title}
                </p>
              </div>
            )
          })}
        </div>
      )}
      {isTabletOrMobile && (
        <div className='row pt-0 justify-content-center'>
          {items.map(item => {
            return (
              <div key={`domain-${item.id}`} className='col-4 py-2'>
                <div
                  className='shadow rounded-circle px-4 py-4 mx-auto'
                  style={{ width: 'fit-content' }}
                >
                  <Image src={item.icon} width='40' height='40' />
                </div>
                <p className='text-uppercase font-weight-bold mx-1 mt-4 mb-3 text-center'>
                  {item.title}
                </p>
              </div>
            )
          })}
        </div>
      )}
    </Section>
  )
}

export default DomainsSection
