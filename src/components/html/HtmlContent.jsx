import { forwardRef } from 'react'

const HtmlContent = forwardRef(({ children, as = 'div', ...props }, ref) => {
  const Element = as
  return (
    <Element
      ref={ref}
      dangerouslySetInnerHTML={{ __html: children }}
      {...props}
    />
  )
})

export default HtmlContent
