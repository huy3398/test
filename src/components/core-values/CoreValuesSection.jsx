import { Col, Row } from 'react-bootstrap'
import Section, { SectionHeader } from '../section'
import { LiteralImage } from '../literal/LiteralImage'
import CoreValuesItem from './CoreValuesItem'

const CoreValues = ({ items, title, image, isTabletOrMobile }) => {
  return (
    <Section className='py-6'>
      <Row>
        <Col lg={6}>
          <SectionHeader align='left'>{title}</SectionHeader>
          {!isTabletOrMobile && (
            <div>
              <LiteralImage src={image} title={title} />
            </div>
          )}
        </Col>
        <Col lg={6} className='d-flex flex-column justify-content-between'>
          {items.map(item => {
            return <CoreValuesItem key={`corevalue-${item.id}`} {...item} />
          })}
        </Col>
      </Row>
    </Section>
  )
}

export default CoreValues
