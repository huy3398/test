import clsx from 'clsx'
import { Media } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'
import styles from './corevalues.module.scss'

const CoreValuesItem = ({ title, description, icon }) => {
  return (
    <Media>
      <div
        className={clsx(
          'border-secondary border rounded-circle d-flex justify-content-center align-items-center shadow',
          styles.imageWrapper,
        )}
      >
        <img src={icon} alt={title} width='auto' />
      </div>
      <Media.Body className='ml-5'>
        <h3 className='text-uppercase h5 font-weight-extra-bolder'>{title}</h3>
        <HtmlContent>{description}</HtmlContent>
      </Media.Body>
    </Media>
  )
}

export default CoreValuesItem
