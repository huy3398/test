import clsx from 'clsx'
import { useContext } from 'react'
import { Accordion, AccordionContext, Card } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'
import styles from './expertise.module.scss'

const ExpertiseCard = ({ eventKey, item, index }) => {
  const currentEventKey = useContext(AccordionContext)
  const isCurrentEventKey = currentEventKey === eventKey

  return (
    <Card
      className={clsx(
        'flex-grow-1 flex-shrink-0 rounded-0',
        styles.item,
        isCurrentEventKey ? 'bg-red-dark' : 'bg-gray-lighter',
      )}
    >
      <Card.Header className='p-0 bg-transparent'>
        <Accordion.Toggle
          as='div'
          className='py-4 text-center'
          role='button'
          eventKey={index.toString()}
        >
          <img
            src={item.icon}
            alt={item.title}
            width='30px'
            height='30px'
            className={clsx({ 'filter-white': isCurrentEventKey })}
          />
          <div
            style={{ fontSize: '10px' }}
            className={clsx(
              'text-uppercase flex-grow-1 align-self-end my-2 font-weight-normal',
              {
                'text-white': isCurrentEventKey,
              },
            )}
          >
            {item.title}
          </div>
        </Accordion.Toggle>
      </Card.Header>
      <Accordion.Collapse eventKey={index.toString()}>
        <Card.Body
          className={clsx('pt-0', {
            'text-white': isCurrentEventKey,
          })}
        >
          <HtmlContent className={styles.itemContentMobile}>
            {item.description}
          </HtmlContent>
        </Card.Body>
      </Accordion.Collapse>
    </Card>
  )
}

export default ExpertiseCard
