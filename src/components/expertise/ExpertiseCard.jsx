import clsx from 'clsx'
import { useContext } from 'react'
import { Accordion, AccordionContext, Card } from 'react-bootstrap'
import HtmlContent from '../html/HtmlContent'
import styles from './expertise.module.scss'

const ExpertiseCard = ({ eventKey, item, index }) => {
  const currentEventKey = useContext(AccordionContext)
  const isCurrentEventKey = currentEventKey === eventKey

  return (
    <Card
      className={clsx(
        'flex-grow-1 flex-shrink-0 rounded-0',
        styles.item,
        isCurrentEventKey ? 'bg-red-dark' : 'bg-gray-lighter',
      )}
    >
      <Card.Header className='p-0 bg-transparent'>
        <Accordion.Toggle
          as='div'
          className='p-4 text-center'
          role='button'
          eventKey={index.toString()}
        >
          <img
            src={item.icon}
            alt={item.title}
            width='60px'
            height='60px'
            className={clsx({ 'filter-white': isCurrentEventKey })}
          />
          <h3
            className={clsx(
              'h4 text-uppercase flex-grow-1 align-self-end my-4',
              {
                'text-white': isCurrentEventKey,
              },
            )}
          >
            {item.title}
          </h3>
        </Accordion.Toggle>
      </Card.Header>
      <Accordion.Collapse eventKey={index.toString()}>
        <Card.Body
          className={clsx('px-5 pt-0', {
            'text-white': isCurrentEventKey,
          })}
        >
          <HtmlContent className={styles.itemContent}>
            {item.description}
          </HtmlContent>
        </Card.Body>
      </Accordion.Collapse>
    </Card>
  )
}

export default ExpertiseCard
