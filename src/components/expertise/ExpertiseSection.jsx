import { Accordion } from 'react-bootstrap'
import clsx from 'clsx'
import Section from '../section'
import styles from './expertise.module.scss'
import ExpertiseCard from './ExpertiseCard'
import ExpertiseMobileCard from './ExpertiseMobileCard'

const ExpertiseSection = ({ items = [], isTabletOrMobile }) => {
  return (
    <Section fluid='xl'>
      <Accordion
        defaultActiveKey='0'
        className={clsx('d-flex align-items-end', styles.expertise)}
      >
        {!isTabletOrMobile &&
          items.map((item, index) => {
            return (
              <ExpertiseCard
                eventKey={index.toString()}
                key={`expertise-${item.id}`}
                index={index}
                item={item}
              />
            )
          })}
        {isTabletOrMobile &&
          items.map((item, index) => {
            return (
              <ExpertiseMobileCard
                eventKey={index.toString()}
                key={`expertise-${item.id}`}
                index={index}
                item={item}
              />
            )
          })}
      </Accordion>
    </Section>
  )
}

export default ExpertiseSection
