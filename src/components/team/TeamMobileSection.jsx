import clsx from 'clsx'
import Carousel from '../3d-carousel/Carousel'
import Section from '../section'
// import TeamItem from './TeamItem'
import styles from './team.module.scss'
import TeamMobileItem from './TeamMobileItem'

const TeamMobileSection = ({ title, items }) => {
  return (
    <Section className={clsx('pt-5 position-relative')} title={title}>
      <div className={clsx(styles.wrapper_mobile, 'mx-auto my-0')}>
        <Carousel>
          {activeIndex =>
            [...items, ...items].map((item, index) => {
              return (
                <TeamMobileItem
                  key={`team-${item.id}`}
                  {...item}
                  isActive={activeIndex === index}
                />
              )
            })
          }
        </Carousel>
      </div>
    </Section>
  )
}

export default TeamMobileSection
