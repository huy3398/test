import clsx from 'clsx'
import Carousel from '../3d-carousel/Carousel'
import Section from '../section'
import TeamItem from './TeamItem'
import styles from './team.module.scss'
import commonStyles from '../section/section.module.scss'

const TeamSection = ({ title, items }) => {
  return (
    <Section
      className={clsx(commonStyles.doubleBackground, 'pt-5 position-relative')}
      title={title}
    >
      <div className={clsx(styles.wrapper, 'mx-auto my-0')}>
        <Carousel>
          {activeIndex =>
            [...items, ...items].map((item, index) => {
              return (
                <TeamItem
                  key={`team-${item.id}`}
                  {...item}
                  isActive={activeIndex === index}
                />
              )
            })
          }
        </Carousel>
      </div>
    </Section>
  )
}

export default TeamSection
