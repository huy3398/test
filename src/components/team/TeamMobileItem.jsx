import clsx from 'clsx'
import Avatar from '../avatar/Avatar'
import HtmlContent from '../html/HtmlContent'
import styles from './team.module.scss'

const TeamMobileItem = ({ image, title, meta, description, isActive }) => {
  return (
    <div className={clsx('text-center', styles.slide_mobile)}>
      <Avatar
        src={image}
        alt={title}
        size={isActive ? 'lg' : 'default'}
        className={clsx('mx-auto', styles.normal, {
          [styles.active]: isActive,
        })}
      />
      <h5
        className={clsx('text-uppercase font-weight-bold', {
          h5: !isActive,
        })}
      >
        {title}
      </h5>
      <p className='lead font-size-sm'>{meta}</p>
      <div className='position-relative'>
        <HtmlContent
          className={clsx(
            {
              [styles.descriptionActive]: isActive,
            },
            'px-3',
          )}
        >
          {description}
        </HtmlContent>
      </div>
    </div>
  )
}

export default TeamMobileItem
