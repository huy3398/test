import clsx from 'clsx'
import { IoLocationOutline } from 'react-icons/io5'
import Section from '../section'
import styles from './locations.module.scss'

const Locations = ({ items, className, isTabletOrMobile }) => {
  return (
    <Section
      className={clsx(className, {
        'py-6': !isTabletOrMobile,
        'pt-4': isTabletOrMobile,
      })}
    >
      <div className='pt-4 row'>
        {items.map(item => {
          return (
            <div
              key={`location-${item.id}`}
              className={clsx(className, {
                'col-md-4 col-12': !isTabletOrMobile,
                'col-md-4 col-12 mb-5': isTabletOrMobile,
              })}
            >
              <div className='px-4 pb-4 pt-5 bg-gray-lighter text-center position-relative'>
                <div
                  className={clsx(
                    'd-flex mb-4 bg-white rounded-circle justify-content-center align-items-center shadow position-absolute',
                    styles.icon,
                  )}
                >
                  <IoLocationOutline className='text-secondary font-size-xxl d-inline-block' />
                </div>
                <h3 className='h5 text-uppercase mx-2 mt-4 mb-3'>
                  {item.title}
                </h3>
                <p className='mx-2'>{item.description}</p>
              </div>
            </div>
          )
        })}
      </div>
    </Section>
  )
}

export default Locations
