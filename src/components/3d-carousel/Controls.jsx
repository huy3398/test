import { Button } from 'react-bootstrap'
import clsx from 'clsx'
import { BsChevronLeft, BsChevronRight } from 'react-icons/bs'
import styles from './carousel.module.scss'

const Arrow = ({ onClick, children, className }) => {
  return (
    <Button
      variant='light'
      className={clsx(
        className,
        styles.arrow,
        'position-absolute bg-white flex-center shadow-sm',
      )}
      onClick={onClick}
    >
      {children}
    </Button>
  )
}

export const NextArrow = ({ onClick }) => {
  return (
    <Arrow className={styles.nextArrow} onClick={onClick}>
      <BsChevronRight className='d-block' size={14} />
    </Arrow>
  )
}

export const PrevArrow = ({ onClick }) => {
  return (
    <Arrow className={styles.prevArrow} onClick={onClick}>
      <BsChevronLeft className='d-block' size={14} />
    </Arrow>
  )
}
