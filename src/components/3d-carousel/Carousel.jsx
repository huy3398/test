import { useState } from 'react'
import Slider from 'react-slick'
import { NextArrow, PrevArrow } from './Controls'

const Carousel = ({ children, slidesToShow = 3, dots = false, rows = 1 }) => {
  const [imageIndex, setImageIndex] = useState(0)

  const settings = {
    autoplay: true,
    autoplaySpeed: 5000,
    centerMode: true,
    infinite: true,
    dots,
    rows,
    speed: 300,
    slidesToShow,
    centerPadding: '0',
    swipeToSlide: true,
    focusOnSelect: true,
    nextArrow: <NextArrow onClick />,
    prevArrow: <PrevArrow onClick />,
    beforeChange: (current, next) => setImageIndex(next),
    responsive: [
      {
        breakpoint: 1490,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 820,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  }

  return <Slider {...settings}>{children(imageIndex)}</Slider>
}

export default Carousel
