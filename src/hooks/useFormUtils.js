import get from 'lodash.get'

export const useFormUtils = formState => {
  const isValid = name => !get(formState, `errors.${name}`)
  const isInvalid = name => !isValid(name)
  const errorMsg = name => get(formState, `errors.${name}.message`)
  const errors = formState.errors
  const isDisabled = formState.isSubmitSuccessful || formState.isSubmitting

  return { isInvalid, isValid, errorMsg, isDisabled, errors }
}
