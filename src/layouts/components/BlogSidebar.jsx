import SearchBox from '../../components/blog/SearchBox'
import CategoriesSection from '../../components/categories/CategoriesSection'

const BlogSidebar = () => {
  return (
    <aside>
      <SearchBox className='mb-5' />
      <CategoriesSection title='Categories' />
    </aside>
  )
}
export default BlogSidebar
