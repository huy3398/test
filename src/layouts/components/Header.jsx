import Link from 'next/link'
import { useRouter } from 'next/router'
import { Container, Nav, Navbar } from 'react-bootstrap'
import Logo from '../../components/logo/Logo'
import routes from './routes'

const Header = () => {
  const { pathname } = useRouter()
  return (
    <header>
      <Container fluid='xl'>
        <Navbar expand='lg'>
          <Navbar.Toggle
            className='bg-light'
            aria-controls='basic-navbar-nav'
          />
          <div className='mx-auto'>
            <Logo as={Navbar.Brand} className='' />
          </div>
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='font-family-display text-uppercase font-weight-medium'>
              {routes.map((route, index) => (
                <Link key={`nav-link-${route.href}`} href={route.href} passHref>
                  <Nav.Link
                    className={{
                      'mr-4': index < routes.length - 1,
                      'mr-0': index === routes.length - 1,
                      'font-weight-bold': pathname === route.href,
                    }}
                  >
                    {route.text}
                  </Nav.Link>
                </Link>
              ))}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </header>
  )
}

export default Header
