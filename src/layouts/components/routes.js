const routes = [
  {
    href: '/about',
    text: 'About Wata',
  },
  {
    href: '/works',
    text: 'Our Works',
  },
  {
    href: '/services',
    text: 'Services',
  },
  {
    href: '/blog',
    text: 'Blog',
  },
  {
    href: '/careers',
    text: 'Careers',
  },
  {
    href: '/contact',
    text: 'Contact Us',
  },
]

export default routes
