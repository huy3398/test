import Link from 'next/link'

const NavLink = ({ href, ...props }) => {
  return (
    <Link href={href} passHref>
      <NavLink {...props} />
    </Link>
  )
}

export default NavLink
