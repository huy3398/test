import getConfig from 'next/config'
import { Container } from 'react-bootstrap'
import { FaFacebookSquare, FaLinkedin } from 'react-icons/fa'
import { BsEnvelope, BsPhone, BsChat } from 'react-icons/bs'
import clsx from 'clsx'
import Link from 'next/link'
import styles from './layout.module.scss'
import Logo from '../../components/logo/Logo'
import { mailto, tel } from '../../utils/url'

const { publicRuntimeConfig: config } = getConfig()

const FooterMobile = () => {
  return (
    <footer className='bg-gray-lighter px-4 pt-4'>
      <Container fluid='xl'>
        <div className='row'>
          <div className='col-7'>
            <Logo className='mb-3' width='75%' />
            <p>
              Hai Au Building, 39B Truong Son, Ward 4, Tan Binh Dist., HCMC,
              Vietnam
            </p>
          </div>
          <div className='col-5'>
            <h3 className='text-uppercase mb-2'>Careers</h3>
            <p>Let&lsquo;s join us and build great things together!</p>
            <Link href='/careers'>
              <a className='btn btn-primary text-uppercase'>Join us</a>
            </Link>
          </div>
        </div>
        <div className='row'>
          <div className='col-7'>
            <h3 className='my-3 h5'>Contact Us</h3>
            <ul className='list-unstyled'>
              <li className='my-3 d-flex align-items-center'>
                <BsEnvelope fontSize='18px' className='text-gray mr-3' />
                <a className={styles.link} href={mailto(config.contact.email)}>
                  {config.contact.email}
                </a>
              </li>
              <li className='my-3 d-flex align-items-center'>
                <BsPhone fontSize='18px' className='text-gray mr-3' />
                <a className={styles.link} href={tel(config.contact.phone)}>
                  {config.contact.phone}
                </a>
              </li>
              <li className='my-3 d-flex align-items-center'>
                <BsChat fontSize='18px' className='text-gray mr-3' />
                <a className={styles.link} href={tel(config.contact.chatUs)}>
                  {config.contact.chatUs}
                </a>
              </li>
              <li className='my-3'>
                {config.contact.linkedin && (
                  <a
                    href={config.contact.linkedin}
                    target='_blank'
                    className='mr-2'
                    rel='noreferrer'
                  >
                    <FaLinkedin fontSize='24px' className='text-linkedin' />
                  </a>
                )}
                {config.contact.facebook && (
                  <a
                    href={config.contact.facebook}
                    target='_blank'
                    className='mr-2'
                    rel='noreferrer'
                  >
                    <FaFacebookSquare
                      fontSize='24px'
                      className='text-facebook'
                    />
                  </a>
                )}
              </li>
            </ul>
          </div>
          <div className='col-5'>
            <h3 className='my-3 h5'>Quick Links</h3>
            <ul className='list-unstyled'>
              <li className='my-3'>
                <Link href='/about'>
                  <a className={clsx(styles.link, 'text-uppercase')}>About</a>
                </Link>
              </li>
              <li className='my-3'>
                <Link href='/services'>
                  <a className={clsx(styles.link, 'text-uppercase')}>
                    Our services
                  </a>
                </Link>
              </li>
              <li className='my-3'>
                <Link href='/works'>
                  <a className={clsx(styles.link, 'text-uppercase')}>
                    Our works
                  </a>
                </Link>
              </li>
              <li className='my-3'>
                <Link href='/blog'>
                  <a className={clsx(styles.link, 'text-uppercase')}>Blog</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </Container>
    </footer>
  )
}

export default FooterMobile
