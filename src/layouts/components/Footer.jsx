import Link from 'next/link'
import getConfig from 'next/config'
import { Container, Row, Col } from 'react-bootstrap'
import {
  FaFacebookSquare,
  FaLinkedin,
  FaSkype,
  FaWhatsappSquare,
} from 'react-icons/fa'
import { BsEnvelope, BsPhone } from 'react-icons/bs'
import clsx from 'clsx'
import styles from './layout.module.scss'
import Logo from '../../components/logo/Logo'
import { mailto, tel, whatsapp } from '../../utils/url'

const { publicRuntimeConfig: config } = getConfig()

const Footer = () => {
  return (
    <footer className={clsx('bg-gray-lighter', styles.footer)}>
      <Container fluid='xl'>
        <Row>
          <Col className='col-md-3'>
            <Logo className='mb-3' />
            <p>
              Hai Au Building, 39B Truong Son, Ward 4, Tan Binh Dist., HCMC,
              Vietnam
            </p>
          </Col>

          <Col className='col-md-3'>
            <h3 className='my-3 h5'>Contact Us</h3>
            <ul className='list-unstyled'>
              <li className='my-3 d-flex align-items-center'>
                <BsEnvelope fontSize='18px' className='text-gray mr-3' />
                <a className={styles.link} href={mailto(config.contact.email)}>
                  {config.contact.email}
                </a>
              </li>
              <li className='my-3 d-flex align-items-center'>
                <BsPhone fontSize='18px' className='text-gray mr-3' />
                <a className={styles.link} href={tel(config.contact.phone)}>
                  {config.contact.phone}
                </a>
              </li>
              <li className='my-3'>
                {config.contact.linkedin && (
                  <a
                    href={config.contact.linkedin}
                    target='_blank'
                    className='mr-2'
                    rel='noreferrer'
                  >
                    <FaLinkedin fontSize='24px' className='text-linkedin' />
                  </a>
                )}
                {config.contact.facebook && (
                  <a
                    href={config.contact.facebook}
                    target='_blank'
                    className='mr-2'
                    rel='noreferrer'
                  >
                    <FaFacebookSquare
                      fontSize='24px'
                      className='text-facebook'
                    />
                  </a>
                )}
                {config.contact.skype && (
                  <a
                    href={config.contact.skype}
                    target='_blank'
                    className='mr-2'
                    rel='noreferrer'
                  >
                    <FaSkype fontSize='24px' className='text-skype' />
                  </a>
                )}
                {config.contact.whatsapp && (
                  <a
                    href={whatsapp(config.contact.whatsapp)}
                    target='_blank'
                    rel='noreferrer'
                  >
                    <FaWhatsappSquare
                      fontSize='24px'
                      className='text-whatsapp'
                    />
                  </a>
                )}
              </li>
            </ul>
          </Col>

          <Col className='col-md-2'>
            <h3 className='my-3 h5'>Quick Links</h3>
            <ul className='list-unstyled'>
              <li className='my-3'>
                <Link href='/about'>
                  <a className={clsx(styles.link, 'text-uppercase')}>About</a>
                </Link>
              </li>
              <li className='my-3'>
                <Link href='/services'>
                  <a className={clsx(styles.link, 'text-uppercase')}>
                    Our services
                  </a>
                </Link>
              </li>
              <li className='my-3'>
                <Link href='/works'>
                  <a className={clsx(styles.link, 'text-uppercase')}>
                    Our works
                  </a>
                </Link>
              </li>
              <li className='my-3'>
                <Link href='/blog'>
                  <a className={clsx(styles.link, 'text-uppercase')}>Blog</a>
                </Link>
              </li>
            </ul>
          </Col>

          <Col className='col-md-2'>
            <h3 className='text-uppercase mt-3 mb-2'>Careers</h3>
            <p>Let&lsquo;s join us and build great things together!</p>
            <Link href='/careers'>
              <a className='btn btn-primary text-uppercase'>Join us</a>
            </Link>
          </Col>
        </Row>
      </Container>
    </footer>
  )
}

export default Footer
