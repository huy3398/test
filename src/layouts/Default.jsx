// import Master from './Master'
import dynamic from 'next/dynamic'

const Master = dynamic(() => import('./Master'), { ssr: false })

const Default = ({ children }) => {
  return (
    <Master>
      <main>{children}</main>
    </Master>
  )
}

export default Default
