import { Col, Container, Row } from 'react-bootstrap'
import { useMediaQuery } from 'react-responsive'
import Banner from '../components/banner/Banner'
import BannerMobile from '../components/banner/BannerMobile'
import BlogSidebar from './components/BlogSidebar'
import Master from './Master'

const Blog = ({ children }) => {
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  return (
    <Master>
      {isTabletOrMobile && <BannerMobile background='/img/banner-blog.jpg' />}
      {!isTabletOrMobile && <Banner background='/img/banner-blog.jpg' />}
      <Container fluid='xl'>
        <Row>
          <Col sm={8} as='main'>
            {children}
          </Col>
          <Col sm={4} as='aside' className='my-5'>
            <BlogSidebar />
          </Col>
        </Row>
      </Container>
    </Master>
  )
}

export default Blog
