import { useMediaQuery } from 'react-responsive'
import Footer from './components/Footer'
import FooterMobile from './components/FooterMobile'
import Header from './components/Header'

const Master = ({ children }) => {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)',
  })
  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

  return (
    <div>
      <Header />
      {children}
      {isDesktopOrLaptop && <Footer />}
      {isTabletOrMobile && <FooterMobile />}
    </div>
  )
}

export default Master
