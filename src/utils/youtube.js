const regex =
  /(?:[?&]vi?=|\/embed\/|\/\d\d?\/|\/vi?\/|https?:\/\/(?:www\.)?youtu\.be\/)([^&\n?#]+)/

export const parse = url => {
  const match = url.match(regex)
  const id = match && match[1].length === 11 ? match[1] : null

  return {
    id,
    embed: `https://www.youtube.com/embed/${id}`,
    thumb: `https://img.youtube.com/vi/${id}/default.jpg`,
  }
}
