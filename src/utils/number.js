export const getTotalPages = (total, pageSize) => {
  const t = total || 0
  return Math.ceil((typeof t === 'string' ? parseInt(t, 10) : t) / pageSize)
}
