export const groupedBy = (items, totalPerRow) => {
  const groupedItems = {}
  items.forEach((item, index) => {
    const groupId = Math.floor(index / totalPerRow)
    if (!groupedItems[groupId]) groupedItems[groupId] = []
    groupedItems[groupId].push(item)
  })

  return Object.keys(groupedItems).map(groupKey => groupedItems[groupKey])
}

export const arrayFrom = number => {
  return Array.from({ length: number }, (_, i) => i + 1)
}
