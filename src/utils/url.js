import qs from 'qs'
import getConfig from 'next/config'

const { publicRuntimeConfig: config } = getConfig()

const urlWithParams = (url, params = {}) => {
  return (
    url +
    qs.stringify(params, {
      addQueryPrefix: true,
    })
  )
}

export const api = (resource, params = {}, local = true) => {
  const url = local
    ? `${config.url.api}/${resource}`
    : `${config.url.apiV2}/${resource}`
  return urlWithParams(url, params)
}

export const apiV2 = (resource, params = {}) => {
  const url = `${config.url.apiV2}/${resource}`
  return urlWithParams(url, params)
}

export const absolute = (path = '', params = {}) => {
  return urlWithParams(config.url.base + path, params)
}

export const sitemap = name => absolute(`/sitemaps/${name}.xml`)

export const blogPost = slug => absolute(`/blog/${slug}`)

export const mailto = email => `mailto:${email}`

export const tel = number => `tel:${number.replace(/[^0-9+]/g, '', '')}`

export const whatsapp = number =>
  `https://wa.me/${number.toString().replace(/[^0-9]/g, '')}`
