import readingTimeLib from 'reading-time'

export const relative = time => {
  const formatter = new Intl.RelativeTimeFormat()
  const milliseconds = new Date().getTime() - time.getTime()

  const minutes = milliseconds / (1000 * 60)
  if (minutes <= 1) return 'just now'
  if (minutes < 60) return formatter.format(Math.floor(minutes), 'minutes')

  const hours = minutes / 60
  if (hours < 24) return formatter.format(Math.floor(hours), 'hours')

  const days = hours / 24
  if (days < 2) return 'yesterday'

  if (days <= 7) return formatter.format(Math.floor(days), 'days')

  const altFormatter = new Intl.DateTimeFormat('en-US', {
    dateStyle: 'long',
  })

  return altFormatter.format(time)
}

export const readingTime = content => {
  return readingTimeLib(content).text
}
