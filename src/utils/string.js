export const strip = (text, letterCount) => {
  const regex = new RegExp(`/^(.{${letterCount}}[^s]*).*/`)
  return text.replace(regex, '$1')
}
