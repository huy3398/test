const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

const conf = require('./config/server.conf')

module.exports = withBundleAnalyzer({
  publicRuntimeConfig: require('./config/public.conf'),
  serverRuntimeConfig: conf,
  future: {
    webpack5: true,
  },
  images: {
    domains: ['i.pravatar.cc'],
  },
  async redirects() {
    return Object.keys(conf.redirects).map(from => {
      return {
        source: from,
        destination: conf.redirects[from],
        permanent: true,
      }
    })
  },
  async rewrites() {
    return [
      { destination: '/api/sitemap/:path', source: '/sitemaps/:path' },
      { destination: '/api/sitemap.xml', source: '/sitemap.xml' },
    ]
  },
})
